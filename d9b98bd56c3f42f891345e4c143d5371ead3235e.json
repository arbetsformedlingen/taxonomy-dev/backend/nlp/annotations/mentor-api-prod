{
  "meta" : {
    "jobad" : {
      "description" : {
        "requirements" : null,
        "needs" : null,
        "company_information" : null,
        "text_formatted" : "Kustbevakningen är en civil statlig myndighet som ansvarar för sjöövervakning och miljöräddningstjänst till sjöss på uppdrag av regering och riksdag. Under beredskapen för miljöräddning arbetar vi förebyggande med miljöövervakning, samtidigt som vi främjar en hållbar havsmiljö. Vi bedriver även tillsyn och kontroll av fiske, tull, gräns, jakt och naturvård till sjöss. Dessutom arbetar vi för att öka tryggheten genom sjötrafikövervakning och har ständig beredskap för sjöräddning. Dygnet runt, året om finns vi på plats längs Sveriges kust, där vi skyddar liv och miljö, nu och för framtiden.\n\n\nKustbevakningen är en arbetsplats som präglas av samarbete och engagemang över områdesgränserna. Här finns engagerade medarbetare med varierande kompetenser vilket skapar en dynamisk arbetsplats med ständiga möjligheter att lära nytt. Kustbevakningen arbetar med och har stort fokus på medarbetarskap och ledarskap och det är därför viktigt att din värdegrund stämmer överens med Kustbevakningens – helhetssyn, välvilja och engagemang. Kustbevakningen erbjuder en attraktiv arbetsplats som utmärks av strävan att vara fri från diskriminering och ge lika möjligheter och rättigheter till alla. \n\n\n\r\n\r\nBeskrivning\nDu ska, i nära samarbete med vår ansvarige för webbförvaltningen, arbeta med att utveckla vår externa webbplats och vårt intranät.\r\n\r\nTillsammans med övriga kommunikatörer arbetar du med stöd i löpande intern- och extern myndighetskommunikation. Det innebär såväl operativt som strategiskt arbete, dock med fokus på webbpublicering/utveckling och digitala medier.\n\nSom webbredaktör arbetar du med de båda webbplatsernas övergripande struktur och för den redaktionella planeringen. Du ansvarar till exempel för den dagliga kvalitetssäkringen. Du ger kvalificerad rådgivning, stöd och vägledning till redaktörer inom myndigheten som ansvarar för publicering inom sina respektive verksamhetsområden. Du tar fram, analyserar och följer upp statistik.\n\nI rollen ingår också att nyhetsvärdera och att använda den redaktionella verktygslådan för att själv skapa innehåll som är anpassat till kanal och målgrupp. En stor del av arbetet ska fokusera på att utveckla de båda webbplatserna; kustbevakningen.se ska bli myndighetens självklara informationskanal, intranätet ska vara ett nav för Kustbevakningens interna kommunikation.\n\nKustbevakningens verksamhet bedrivs dygnet runt året om och det ställer krav på en aktiv, öppen, tydlig och lyhörd kommunikation, både externt och internt. Kriskommunikation ingår också i arbetet.\n\nTjänsten finns vid kommunikationsenheten och du kommer att vara placerad i Karlskrona. Idag arbetar fem personer på kommunikationsenheten som leds från Stockholm, men även har medarbetare i Göteborg och Karlskrona. Tjänsten är en tillsvidareanställning.\n\nArbetsuppgifter\nDu har en viktig roll i att jobba med innehåll och budskap i relevanta kanaler och att stödja verksamhetsutveckling genom att ta fram kommunikationsstrategier och planer.\n\nDina huvudsakliga arbetsuppgifter blir att:\n\nArbeta med utveckling och förvaltning av innehållet på Kustbevakningens externa webbplats och intranät. Delta i kommunikationsenhetens övergripande arbete som exempelvis att producera och genomföra kommunikationsinsatser via text, bild, och film till alla förekommande kommunikationskanaler. Vi ser gärna att du är van att arbeta med sociala kanaler.\n\nKvalifikationer\nDu har högskoleutbildning inom kommunikationsområdet eller annan för anställningen, som vi bedömer, likvärdig utbildning. Du har mycket god förmåga att kommunicera och uttrycka dig i tal och skrift, på svenska och engelska. Du har minst fem års erfarenhet av kvalificerat och brett kommunikationsarbete; såväl att ta fram kommunikationsplaner och strategier som att praktiskt arbeta med innehåll i olika kanaler. Du har arbetat under minst två år med att utveckla webbplatser.\n\nDu har arbetat inom offentlig sektor med motsvarande arbetsuppgifter och du har goda kunskaper i EpiServer, Adobe Creative Suite och MS Office.\n\nErfarenhet av arbete med\n\n - webbanalys/statistik-verktyg\n - grafisk design\n - produktion av rörlig media\n - tillgänglighetsanpassning i praktiken\n - arbete inom offentlig förvaltning\n\nDet är meriterande om du arbetat med liknande arbetsuppgifter vid en offentlig, operativ verksamhet med erfarenhet av övergripande frågor inom totalförsvar och samhällets krisberedskap.\n\nVi söker dig som har en väl utvecklad samarbetsförmåga, är bra på att lyssna och är lyhörd för verksamhetens behov. Du är van vid att arbeta självständigt, tar initiativ och ansvar i ditt arbete. Du hittar rätt kommunikationsperspektiv även i stressade situationer och tycker om ett omväxlande arbete.\n\nVi lägger stor vikt vid personliga egenskaper.\n\nVillkor\nTjänsten är en tillsvidareanställning med provanställning och tillträde 1 november 2022 eller enligt överenskommelse.\n\nFör anställning krävs svenskt medborgarskap, körkort för personbil, drogtest samt säkerhetsprövning enligt säkerhetsskyddslagen.\n\nKustbevakningen medverkar i utlandsuppdrag och internationella insatser. I anställningen ingår därför en skyldighet att tjänstgöra utomlands i de fall det krävs för verksamheten.\n\nI samband med operationer beordras ofta övertidsarbete dvs arbetet utanför ordinarie arbetstid.\n\nÖvrigt\n\n\nFör att kvalitetssäkra och effektivisera rekryteringsarbetet använder vi ett elektroniskt ansökningsformulär i samarbete med Varbi för samtliga lediga jobb. Fyll i din ansökan så omsorgsfullt som möjligt för att ge en rättvisande bild av dig själv.\n\n\nKustbevakningen är en statlig myndighet och det innebär att uppgifterna som du lämnar till oss kan komma att lämnas ut enligt offentlighetsprincipen. Normalt gäller ingen sekretess för de uppgifter som behandlas i databasen. Om du har skyddade personuppgifter ska du kontakta den kontaktperson som finns angiven i annonsen.\n\n\nInför rekryteringsarbetet har Kustbevakningen tagit ställning till rekryteringskanaler och marknadsföring. Vi undanber oss därför bestämt kontakt med mediesäljare, rekryteringssajter och liknande. ",
        "conditions" : "100. Tillträde: Enligt överenskommelse\r\nTillsvidareanställning",
        "text" : "Kustbevakningen är en civil statlig myndighet som ansvarar för sjöövervakning och miljöräddningstjänst till sjöss på uppdrag av regering och riksdag. Under beredskapen för miljöräddning arbetar vi förebyggande med miljöövervakning, samtidigt som vi främjar en hållbar havsmiljö. Vi bedriver även tillsyn och kontroll av fiske, tull, gräns, jakt och naturvård till sjöss. Dessutom arbetar vi för att öka tryggheten genom sjötrafikövervakning och har ständig beredskap för sjöräddning. Dygnet runt, året om finns vi på plats längs Sveriges kust, där vi skyddar liv och miljö, nu och för framtiden.\n\n\nKustbevakningen är en arbetsplats som präglas av samarbete och engagemang över områdesgränserna. Här finns engagerade medarbetare med varierande kompetenser vilket skapar en dynamisk arbetsplats med ständiga möjligheter att lära nytt. Kustbevakningen arbetar med och har stort fokus på medarbetarskap och ledarskap och det är därför viktigt att din värdegrund stämmer överens med Kustbevakningens – helhetssyn, välvilja och engagemang. Kustbevakningen erbjuder en attraktiv arbetsplats som utmärks av strävan att vara fri från diskriminering och ge lika möjligheter och rättigheter till alla. \n\n\n\r\n\r\nBeskrivning\nDu ska, i nära samarbete med vår ansvarige för webbförvaltningen, arbeta med att utveckla vår externa webbplats och vårt intranät.\r\n\r\nTillsammans med övriga kommunikatörer arbetar du med stöd i löpande intern- och extern myndighetskommunikation. Det innebär såväl operativt som strategiskt arbete, dock med fokus på webbpublicering/utveckling och digitala medier.\n\nSom webbredaktör arbetar du med de båda webbplatsernas övergripande struktur och för den redaktionella planeringen. Du ansvarar till exempel för den dagliga kvalitetssäkringen. Du ger kvalificerad rådgivning, stöd och vägledning till redaktörer inom myndigheten som ansvarar för publicering inom sina respektive verksamhetsområden. Du tar fram, analyserar och följer upp statistik.\n\nI rollen ingår också att nyhetsvärdera och att använda den redaktionella verktygslådan för att själv skapa innehåll som är anpassat till kanal och målgrupp. En stor del av arbetet ska fokusera på att utveckla de båda webbplatserna; kustbevakningen.se ska bli myndighetens självklara informationskanal, intranätet ska vara ett nav för Kustbevakningens interna kommunikation.\n\nKustbevakningens verksamhet bedrivs dygnet runt året om och det ställer krav på en aktiv, öppen, tydlig och lyhörd kommunikation, både externt och internt. Kriskommunikation ingår också i arbetet.\n\nTjänsten finns vid kommunikationsenheten och du kommer att vara placerad i Karlskrona. Idag arbetar fem personer på kommunikationsenheten som leds från Stockholm, men även har medarbetare i Göteborg och Karlskrona. Tjänsten är en tillsvidareanställning.\n\nArbetsuppgifter\nDu har en viktig roll i att jobba med innehåll och budskap i relevanta kanaler och att stödja verksamhetsutveckling genom att ta fram kommunikationsstrategier och planer.\n\nDina huvudsakliga arbetsuppgifter blir att:\n\nArbeta med utveckling och förvaltning av innehållet på Kustbevakningens externa webbplats och intranät. Delta i kommunikationsenhetens övergripande arbete som exempelvis att producera och genomföra kommunikationsinsatser via text, bild, och film till alla förekommande kommunikationskanaler. Vi ser gärna att du är van att arbeta med sociala kanaler.\n\nKvalifikationer\nDu har högskoleutbildning inom kommunikationsområdet eller annan för anställningen, som vi bedömer, likvärdig utbildning. Du har mycket god förmåga att kommunicera och uttrycka dig i tal och skrift, på svenska och engelska. Du har minst fem års erfarenhet av kvalificerat och brett kommunikationsarbete; såväl att ta fram kommunikationsplaner och strategier som att praktiskt arbeta med innehåll i olika kanaler. Du har arbetat under minst två år med att utveckla webbplatser.\n\nDu har arbetat inom offentlig sektor med motsvarande arbetsuppgifter och du har goda kunskaper i EpiServer, Adobe Creative Suite och MS Office.\n\nErfarenhet av arbete med\n\n - webbanalys/statistik-verktyg\n - grafisk design\n - produktion av rörlig media\n - tillgänglighetsanpassning i praktiken\n - arbete inom offentlig förvaltning\n\nDet är meriterande om du arbetat med liknande arbetsuppgifter vid en offentlig, operativ verksamhet med erfarenhet av övergripande frågor inom totalförsvar och samhällets krisberedskap.\n\nVi söker dig som har en väl utvecklad samarbetsförmåga, är bra på att lyssna och är lyhörd för verksamhetens behov. Du är van vid att arbeta självständigt, tar initiativ och ansvar i ditt arbete. Du hittar rätt kommunikationsperspektiv även i stressade situationer och tycker om ett omväxlande arbete.\n\nVi lägger stor vikt vid personliga egenskaper.\n\nVillkor\nTjänsten är en tillsvidareanställning med provanställning och tillträde 1 november 2022 eller enligt överenskommelse.\n\nFör anställning krävs svenskt medborgarskap, körkort för personbil, drogtest samt säkerhetsprövning enligt säkerhetsskyddslagen.\n\nKustbevakningen medverkar i utlandsuppdrag och internationella insatser. I anställningen ingår därför en skyldighet att tjänstgöra utomlands i de fall det krävs för verksamheten.\n\nI samband med operationer beordras ofta övertidsarbete dvs arbetet utanför ordinarie arbetstid.\n\nÖvrigt\n\n\nFör att kvalitetssäkra och effektivisera rekryteringsarbetet använder vi ett elektroniskt ansökningsformulär i samarbete med Varbi för samtliga lediga jobb. Fyll i din ansökan så omsorgsfullt som möjligt för att ge en rättvisande bild av dig själv.\n\n\nKustbevakningen är en statlig myndighet och det innebär att uppgifterna som du lämnar till oss kan komma att lämnas ut enligt offentlighetsprincipen. Normalt gäller ingen sekretess för de uppgifter som behandlas i databasen. Om du har skyddade personuppgifter ska du kontakta den kontaktperson som finns angiven i annonsen.\n\n\nInför rekryteringsarbetet har Kustbevakningen tagit ställning till rekryteringskanaler och marknadsföring. Vi undanber oss därför bestämt kontakt med mediesäljare, rekryteringssajter och liknande."
      },
      "headline" : "Erfaren webbredaktör till Kustbevakningen i Karlskrona",
      "driving_license" : [ {
        "concept_id" : "VTK8_WRx_GcM",
        "label" : "B",
        "legacy_ams_taxonomy_id" : "3"
      } ],
      "employer" : {
        "email" : null,
        "phone_number" : null,
        "organization_number" : "2021003997",
        "name" : "Kustbevakningen",
        "url" : null,
        "workplace" : "Kustbevakningen"
      },
      "removed_date" : null,
      "logo_url" : "https://www.arbetsformedlingen.se/rest/arbetsgivare/rest/af/v3/organisation/2021003997/logotyper/logo.png",
      "last_publication_date" : "2022-09-05T23:59:59",
      "number_of_vacancies" : 1,
      "occupation" : {
        "concept_id" : "b3Jk_Gfs_oo9",
        "label" : "Webbredaktör",
        "legacy_ams_taxonomy_id" : "6739"
      },
      "occupation_group" : {
        "concept_id" : "SgNH_hag_n9D",
        "label" : "Journalister m.fl.",
        "legacy_ams_taxonomy_id" : "2642"
      },
      "employment_type" : {
        "concept_id" : "PFZr_Syz_cUq",
        "label" : "Vanlig anställning",
        "legacy_ams_taxonomy_id" : "1"
      },
      "webpage_url" : "https://arbetsformedlingen.se/platsbanken/annonser/26355073",
      "driving_license_required" : true,
      "application_contacts" : [ {
        "description" : "Eva Lindé",
        "email" : null,
        "telephone" : "0708-406279",
        "name" : null,
        "contact_type" : null
      } ],
      "duration" : {
        "concept_id" : "a7uU_j21_mkL",
        "label" : "Tills vidare",
        "legacy_ams_taxonomy_id" : "1"
      },
      "nice_to_have" : {
        "education_level" : [ ],
        "work_experiences" : [ ],
        "skills" : [ ],
        "education" : [ ],
        "languages" : [ ]
      },
      "application_details" : {
        "via_af" : false,
        "email" : null,
        "other" : null,
        "reference" : "2022/184",
        "url" : "https://kustbevakningen.varbi.com/what:job/jobID:531759/type:job/where:1/apply:1",
        "information" : null
      },
      "source_type" : "VIA_PLATSBANKEN_DXA",
      "application_deadline" : "2022-09-05T23:59:59",
      "external_id" : "46-202100-3997-531759",
      "workplace_address" : {
        "coordinates" : [ 15.5869, 56.161224 ],
        "city" : null,
        "postcode" : null,
        "municipality" : "Karlskrona",
        "region_concept_id" : "DQZd_uYs_oKb",
        "region" : "Blekinge län",
        "country_code" : "199",
        "region_code" : "10",
        "municipality_concept_id" : "YSt4_bAa_ccs",
        "country_concept_id" : "i46j_HmG_v64",
        "municipality_code" : "1080",
        "street_address" : null,
        "country" : "Sverige"
      },
      "id" : "26355073",
      "experience_required" : true,
      "working_hours_type" : {
        "concept_id" : "6YE1_gAC_R2G",
        "label" : "Heltid",
        "legacy_ams_taxonomy_id" : "1"
      },
      "access" : null,
      "timestamp" : 1658836633612,
      "must_have" : {
        "education_level" : [ ],
        "work_experiences" : [ {
          "concept_id" : "b3Jk_Gfs_oo9",
          "weight" : 10,
          "label" : "Webbredaktör",
          "legacy_ams_taxonomy_id" : "6739"
        } ],
        "skills" : [ ],
        "education" : [ ],
        "languages" : [ ]
      },
      "occupation_field" : {
        "concept_id" : "9puE_nYg_crq",
        "label" : "Kultur, media, design",
        "legacy_ams_taxonomy_id" : "11"
      },
      "scope_of_work" : {
        "min" : 100,
        "max" : 100
      },
      "salary_type" : {
        "concept_id" : "oG8G_9cW_nRf",
        "label" : "Fast månads- vecko- eller timlön",
        "legacy_ams_taxonomy_id" : "1"
      },
      "removed" : false,
      "salary_description" : "Månadslön",
      "access_to_own_car" : false,
      "publication_date" : "2022-07-26T13:57:13"
    },
    "name" : "Erfaren webbredaktör till Kustbevakningen i Karlskrona",
    "user" : "kristof.fischer"
  },
  "sha1" : "d9b98bd56c3f42f891345e4c143d5371ead3235e",
  "annotations" : [ {
    "preferred-label" : "Webbredaktör",
    "sentiment" : "MUST_HAVE",
    "type" : "occupation-name",
    "start-position" : 8,
    "end-position" : 20,
    "concept-id" : "b3Jk_Gfs_oo9",
    "matched-string" : "webbredaktör",
    "annotation-id" : 0
  }, {
    "preferred-label" : "Karlskrona",
    "type" : "municipality",
    "start-position" : 44,
    "end-position" : 54,
    "concept-id" : "YSt4_bAa_ccs",
    "matched-string" : "Karlskrona",
    "annotation-id" : 16
  }, {
    "preferred-label" : "Sverige",
    "type" : "country",
    "start-position" : 584,
    "end-position" : 592,
    "concept-id" : "i46j_HmG_v64",
    "matched-string" : "Sveriges",
    "annotation-id" : 17
  }, {
    "preferred-label" : "Webbredaktör",
    "sentiment" : "MUST_HAVE",
    "type" : "occupation-name",
    "start-position" : 1634,
    "end-position" : 1646,
    "concept-id" : "b3Jk_Gfs_oo9",
    "matched-string" : "webbredaktör",
    "annotation-id" : 3
  }, {
    "preferred-label" : "Karlskrona",
    "type" : "municipality",
    "start-position" : 2661,
    "end-position" : 2671,
    "concept-id" : "YSt4_bAa_ccs",
    "matched-string" : "Karlskrona",
    "annotation-id" : 18
  }, {
    "preferred-label" : "Stockholm",
    "type" : "municipality",
    "start-position" : 2738,
    "end-position" : 2747,
    "concept-id" : "AvNB_uwa_6n6",
    "matched-string" : "Stockholm",
    "annotation-id" : 19
  }, {
    "preferred-label" : "Göteborg",
    "type" : "municipality",
    "start-position" : 2776,
    "end-position" : 2784,
    "concept-id" : "PVZL_BQT_XtL",
    "matched-string" : "Göteborg",
    "annotation-id" : 20
  }, {
    "preferred-label" : "Karlskrona",
    "type" : "municipality",
    "start-position" : 2789,
    "end-position" : 2799,
    "concept-id" : "YSt4_bAa_ccs",
    "matched-string" : "Karlskrona",
    "annotation-id" : 21
  }, {
    "preferred-label" : "Tillsvidareanställning (inkl. eventuell provanställning)",
    "type" : "employment-type",
    "start-position" : 2816,
    "end-position" : 2838,
    "concept-id" : "kpPX_CNN_gDU",
    "matched-string" : "tillsvidareanställning",
    "annotation-id" : 22
  }, {
    "preferred-label" : "Publicering, webben och sociala medier",
    "sentiment" : "MUST_HAVE",
    "type" : "skill",
    "start-position" : 3408,
    "end-position" : 3423,
    "concept-id" : "k29D_fT4_Wso",
    "matched-string" : "sociala kanaler",
    "annotation-id" : 23
  }, {
    "preferred-label" : "Universitets- eller högskoleutbildning",
    "sentiment" : "MUST_HAVE",
    "type" : "sni-level-4",
    "start-position" : 3449,
    "end-position" : 3467,
    "concept-id" : "m1vC_Ssq_5Y7",
    "matched-string" : "högskoleutbildning",
    "annotation-id" : 24
  }, {
    "preferred-label" : "Informations- och kommunikationsteknik (IKT), allmän utbildning",
    "sentiment" : "MUST_HAVE",
    "type" : "sun-education-field-4",
    "start-position" : 3473,
    "end-position" : 3494,
    "concept-id" : "Wswh_P8P_qEL",
    "matched-string" : "kommunikationsområdet",
    "annotation-id" : 25
  }, {
    "preferred-label" : "Svenska",
    "sentiment" : "MUST_HAVE",
    "type" : "language",
    "start-position" : 3644,
    "end-position" : 3651,
    "concept-id" : "zSLA_vw2_FXN",
    "matched-string" : "svenska",
    "annotation-id" : 5
  }, {
    "preferred-label" : "Engelska",
    "sentiment" : "MUST_HAVE",
    "type" : "language",
    "start-position" : 3656,
    "end-position" : 3664,
    "concept-id" : "NVxJ_hLg_TYS",
    "matched-string" : "engelska",
    "annotation-id" : 6
  }, {
    "preferred-label" : "5 års erfarenhet eller mer",
    "sentiment" : "MUST_HAVE",
    "type" : "occupation-experience-year",
    "start-position" : 3673,
    "end-position" : 3697,
    "concept-id" : "8CVf_zRX_aZj",
    "matched-string" : "minst fem års erfarenhet",
    "annotation-id" : 26
  }, {
    "preferred-label" : null,
    "sentiment" : "MUST_HAVE",
    "type" : "skill",
    "start-position" : 3724,
    "end-position" : 3744,
    "concept-id" : null,
    "matched-string" : "kommunikationsarbete",
    "annotation-id" : 27
  }, {
    "preferred-label" : "2-4 års erfarenhet",
    "sentiment" : "MUST_HAVE",
    "type" : "occupation-experience-year",
    "start-position" : 3876,
    "end-position" : 3888,
    "concept-id" : "RWA2_uu6_udD",
    "matched-string" : "minst två år",
    "annotation-id" : 28
  }, {
    "preferred-label" : "Webbutveckling",
    "sentiment" : "MUST_HAVE",
    "type" : "skill",
    "start-position" : 3897,
    "end-position" : 3917,
    "concept-id" : "AfJ4_Pvv_YBa",
    "matched-string" : "utveckla webbplatser",
    "annotation-id" : 29
  }, {
    "preferred-label" : "EPiServer, publicerings- och portalverktyg",
    "sentiment" : "MUST_HAVE",
    "type" : "skill",
    "start-position" : 4017,
    "end-position" : 4026,
    "concept-id" : "CTTL_357_Ld2",
    "matched-string" : "episerver",
    "annotation-id" : 7
  }, {
    "preferred-label" : "Layout-Adobe Creative Suite",
    "sentiment" : "MUST_HAVE",
    "type" : "skill",
    "start-position" : 4028,
    "end-position" : 4048,
    "concept-id" : "qK2A_9oq_zx6",
    "matched-string" : "Adobe Creative Suite",
    "annotation-id" : 30
  }, {
    "preferred-label" : "MS Office",
    "sentiment" : "MUST_HAVE",
    "type" : "skill",
    "start-position" : 4053,
    "end-position" : 4062,
    "concept-id" : "w8bU_wHe_omF",
    "matched-string" : "ms office",
    "annotation-id" : 8
  }, {
    "preferred-label" : "webbanalys",
    "sentiment" : "MUST_HAVE",
    "type" : "esco-skill",
    "start-position" : 4094,
    "end-position" : 4104,
    "concept-id" : "oFJV_raT_EUx",
    "matched-string" : "webbanalys",
    "annotation-id" : 31
  }, {
    "preferred-label" : "statistik",
    "sentiment" : "MUST_HAVE",
    "type" : "esco-skill",
    "start-position" : 4105,
    "end-position" : 4122,
    "concept-id" : "9qyj_yZN_P6Q",
    "matched-string" : "statistik-verktyg",
    "annotation-id" : 32
  }, {
    "preferred-label" : "Grafisk design",
    "sentiment" : "MUST_HAVE",
    "type" : "skill",
    "start-position" : 4126,
    "end-position" : 4140,
    "concept-id" : "dTK5_R7G_p56",
    "matched-string" : "grafisk design",
    "annotation-id" : 11
  }, {
    "preferred-label" : "Rörlig bild",
    "sentiment" : "MUST_HAVE",
    "type" : "skill",
    "start-position" : 4144,
    "end-position" : 4170,
    "concept-id" : "sjcy_d9R_ZGE",
    "matched-string" : "produktion av rörlig media",
    "annotation-id" : 33
  }, {
    "preferred-label" : null,
    "sentiment" : "MUST_HAVE",
    "type" : "skill",
    "start-position" : 4174,
    "end-position" : 4199,
    "concept-id" : null,
    "matched-string" : "tillgänglighetsanpassning",
    "annotation-id" : 34
  }, {
    "preferred-label" : null,
    "sentiment" : "MUST_HAVE",
    "type" : "skill",
    "start-position" : 4227,
    "comment" : "finns bara som keyword",
    "end-position" : 4248,
    "concept-id" : null,
    "matched-string" : "offentlig förvaltning",
    "annotation-id" : 12
  }, {
    "preferred-label" : null,
    "sentiment" : "NICE_TO_HAVE",
    "type" : "skill",
    "start-position" : 4393,
    "end-position" : 4405,
    "concept-id" : null,
    "matched-string" : "totalförsvar",
    "annotation-id" : 35
  }, {
    "preferred-label" : "samarbeta med kolleger",
    "sentiment" : "MUST_HAVE",
    "type" : "esco-skill",
    "start-position" : 4475,
    "end-position" : 4491,
    "concept-id" : "X15R_797_q62",
    "matched-string" : "samarbetsförmåga",
    "annotation-id" : 36
  }, {
    "preferred-label" : "arbeta självständigt",
    "sentiment" : "MUST_HAVE",
    "type" : "esco-skill",
    "start-position" : 4571,
    "end-position" : 4591,
    "concept-id" : "NyFu_HHd_zhz",
    "matched-string" : "arbeta självständigt",
    "annotation-id" : 37
  }, {
    "preferred-label" : "visa ansvarstagande",
    "sentiment" : "MUST_HAVE",
    "type" : "esco-skill",
    "start-position" : 4611,
    "end-position" : 4617,
    "concept-id" : "bcRp_hxV_f5G",
    "matched-string" : "ansvar",
    "annotation-id" : 38
  }, {
    "preferred-label" : "Tillsvidareanställning (inkl. eventuell provanställning)",
    "type" : "employment-type",
    "start-position" : 4811,
    "end-position" : 4833,
    "concept-id" : "kpPX_CNN_gDU",
    "matched-string" : "tillsvidareanställning",
    "annotation-id" : 39
  }, {
    "preferred-label" : "Svenskt medborgarskap",
    "sentiment" : "MUST_HAVE",
    "type" : "skill",
    "start-position" : 4937,
    "end-position" : 4958,
    "concept-id" : "chKz_HpD_XL5",
    "matched-string" : "svenskt medborgarskap",
    "annotation-id" : 14
  }, {
    "preferred-label" : "B",
    "sentiment" : "MUST_HAVE",
    "type" : "driving-licence",
    "start-position" : 4960,
    "end-position" : 4981,
    "concept-id" : "VTK8_WRx_GcM",
    "matched-string" : "körkort för personbil",
    "annotation-id" : 40
  } ],
  "text" : "Erfaren webbredaktör till Kustbevakningen i Karlskrona\nKustbevakningen är en civil statlig myndighet som ansvarar för sjöövervakning och miljöräddningstjänst till sjöss på uppdrag av regering och riksdag. Under beredskapen för miljöräddning arbetar vi förebyggande med miljöövervakning, samtidigt som vi främjar en hållbar havsmiljö. Vi bedriver även tillsyn och kontroll av fiske, tull, gräns, jakt och naturvård till sjöss. Dessutom arbetar vi för att öka tryggheten genom sjötrafikövervakning och har ständig beredskap för sjöräddning. Dygnet runt, året om finns vi på plats längs Sveriges kust, där vi skyddar liv och miljö, nu och för framtiden.\n\n\nKustbevakningen är en arbetsplats som präglas av samarbete och engagemang över områdesgränserna. Här finns engagerade medarbetare med varierande kompetenser vilket skapar en dynamisk arbetsplats med ständiga möjligheter att lära nytt. Kustbevakningen arbetar med och har stort fokus på medarbetarskap och ledarskap och det är därför viktigt att din värdegrund stämmer överens med Kustbevakningens – helhetssyn, välvilja och engagemang. Kustbevakningen erbjuder en attraktiv arbetsplats som utmärks av strävan att vara fri från diskriminering och ge lika möjligheter och rättigheter till alla. \n\n\n\r\n\r\nBeskrivning\nDu ska, i nära samarbete med vår ansvarige för webbförvaltningen, arbeta med att utveckla vår externa webbplats och vårt intranät.\r\n\r\nTillsammans med övriga kommunikatörer arbetar du med stöd i löpande intern- och extern myndighetskommunikation. Det innebär såväl operativt som strategiskt arbete, dock med fokus på webbpublicering/utveckling och digitala medier.\n\nSom webbredaktör arbetar du med de båda webbplatsernas övergripande struktur och för den redaktionella planeringen. Du ansvarar till exempel för den dagliga kvalitetssäkringen. Du ger kvalificerad rådgivning, stöd och vägledning till redaktörer inom myndigheten som ansvarar för publicering inom sina respektive verksamhetsområden. Du tar fram, analyserar och följer upp statistik.\n\nI rollen ingår också att nyhetsvärdera och att använda den redaktionella verktygslådan för att själv skapa innehåll som är anpassat till kanal och målgrupp. En stor del av arbetet ska fokusera på att utveckla de båda webbplatserna; kustbevakningen.se ska bli myndighetens självklara informationskanal, intranätet ska vara ett nav för Kustbevakningens interna kommunikation.\n\nKustbevakningens verksamhet bedrivs dygnet runt året om och det ställer krav på en aktiv, öppen, tydlig och lyhörd kommunikation, både externt och internt. Kriskommunikation ingår också i arbetet.\n\nTjänsten finns vid kommunikationsenheten och du kommer att vara placerad i Karlskrona. Idag arbetar fem personer på kommunikationsenheten som leds från Stockholm, men även har medarbetare i Göteborg och Karlskrona. Tjänsten är en tillsvidareanställning.\n\nArbetsuppgifter\nDu har en viktig roll i att jobba med innehåll och budskap i relevanta kanaler och att stödja verksamhetsutveckling genom att ta fram kommunikationsstrategier och planer.\n\nDina huvudsakliga arbetsuppgifter blir att:\n\nArbeta med utveckling och förvaltning av innehållet på Kustbevakningens externa webbplats och intranät. Delta i kommunikationsenhetens övergripande arbete som exempelvis att producera och genomföra kommunikationsinsatser via text, bild, och film till alla förekommande kommunikationskanaler. Vi ser gärna att du är van att arbeta med sociala kanaler.\n\nKvalifikationer\nDu har högskoleutbildning inom kommunikationsområdet eller annan för anställningen, som vi bedömer, likvärdig utbildning. Du har mycket god förmåga att kommunicera och uttrycka dig i tal och skrift, på svenska och engelska. Du har minst fem års erfarenhet av kvalificerat och brett kommunikationsarbete; såväl att ta fram kommunikationsplaner och strategier som att praktiskt arbeta med innehåll i olika kanaler. Du har arbetat under minst två år med att utveckla webbplatser.\n\nDu har arbetat inom offentlig sektor med motsvarande arbetsuppgifter och du har goda kunskaper i EpiServer, Adobe Creative Suite och MS Office.\n\nErfarenhet av arbete med\n\n - webbanalys/statistik-verktyg\n - grafisk design\n - produktion av rörlig media\n - tillgänglighetsanpassning i praktiken\n - arbete inom offentlig förvaltning\n\nDet är meriterande om du arbetat med liknande arbetsuppgifter vid en offentlig, operativ verksamhet med erfarenhet av övergripande frågor inom totalförsvar och samhällets krisberedskap.\n\nVi söker dig som har en väl utvecklad samarbetsförmåga, är bra på att lyssna och är lyhörd för verksamhetens behov. Du är van vid att arbeta självständigt, tar initiativ och ansvar i ditt arbete. Du hittar rätt kommunikationsperspektiv även i stressade situationer och tycker om ett omväxlande arbete.\n\nVi lägger stor vikt vid personliga egenskaper.\n\nVillkor\nTjänsten är en tillsvidareanställning med provanställning och tillträde 1 november 2022 eller enligt överenskommelse.\n\nFör anställning krävs svenskt medborgarskap, körkort för personbil, drogtest samt säkerhetsprövning enligt säkerhetsskyddslagen.\n\nKustbevakningen medverkar i utlandsuppdrag och internationella insatser. I anställningen ingår därför en skyldighet att tjänstgöra utomlands i de fall det krävs för verksamheten.\n\nI samband med operationer beordras ofta övertidsarbete dvs arbetet utanför ordinarie arbetstid.\n\nÖvrigt\n\n\nFör att kvalitetssäkra och effektivisera rekryteringsarbetet använder vi ett elektroniskt ansökningsformulär i samarbete med Varbi för samtliga lediga jobb. Fyll i din ansökan så omsorgsfullt som möjligt för att ge en rättvisande bild av dig själv.\n\n\nKustbevakningen är en statlig myndighet och det innebär att uppgifterna som du lämnar till oss kan komma att lämnas ut enligt offentlighetsprincipen. Normalt gäller ingen sekretess för de uppgifter som behandlas i databasen. Om du har skyddade personuppgifter ska du kontakta den kontaktperson som finns angiven i annonsen.\n\n\nInför rekryteringsarbetet har Kustbevakningen tagit ställning till rekryteringskanaler och marknadsföring. Vi undanber oss därför bestämt kontakt med mediesäljare, rekryteringssajter och liknande."
}