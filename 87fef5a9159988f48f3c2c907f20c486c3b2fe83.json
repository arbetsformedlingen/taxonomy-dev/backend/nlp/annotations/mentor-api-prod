{
  "meta" : {
    "jobad" : {
      "description" : {
        "requirements" : null,
        "needs" : null,
        "company_information" : null,
        "text_formatted" : "Högskolan för lärande och kommunikation (HLK) är en av fyra fackhögskolor vid Jönköping University.\r\n\r\nVi bedriver utbildning och forskning inom medie- och kommunikationsvetenskap, internationellt arbete, Human Resources och lärarutbildning, och har en omfattande uppdragsverksamhet riktad mot fortbildning och kompetensutveckling. Hos oss kommer du att få uppleva en kreativ miljö med goda möjligheter till eget inflytande och personlig utveckling.\n\nAvdelningen för kommunikation och beteendevetenskaper rymmer ämnena medie- och kommunikationsvetenskap, psykologi samt forskningsmiljön CHILD. Inom medie- och kommunikationsvetenskap finns utbildningarna Medie- och kommunikationsvetenskapliga programmet (kandidat), Sustainable Communication (internationell magister/master) samt forskarutbildning. Den medie- och kommunikationsvetenskapliga forskningen har haft en internationell prägel och, i linje med utbildningen på avancerad nivå, en profil mot hållbarhet och kommunikation. Det medie- och kommunikationsvetenskapliga kollegiet består av 20-talet medarbetare och inrymmer både akademisk och praktisk kompetens.\n\n \n\nVad erbjuder vi?\n\nSom professor är dina främsta arbetsuppgifter att arbeta med forskning och utveckling av den medie- och kommunikationsvetenskapliga forskningsmiljön samt bidra till den strategiska utvecklingen av forskningen inom fackhögskolan, genom samverkan såväl inom JU som regionalt, nationellt och internationellt. Du förväntas kunna handleda doktorander samt handleda och examinera examensarbeten på grundnivå och avancerad nivå, samt undervisa på de olika medie- och kommunikationsvetenskapliga utbildningarna. I arbetsuppgifterna ingår även utveckling av utbildningar inom ämnet, administrativt arbete samt ämnesreprepresentation i HLK:s forskningsnämnd och utökade ledningsråd.\n\n \n\nVem söker vi?\n\nVi söker dig som har en doktorsexamen inom relevant område och som kan uppvisa en forskningsprofil med tydlig progression efter avhandlingsarbetet vad gäller bredd och djup inom ämnesområdet samt arbeten som är av god internationell standard. Du ska vara vetenskapligt skicklig med avseende på självständiga forskningsinsatser, varit ledare för forskningsgrupp, ha förmåga att planera och leda forskning samt förmåga att kommunicera forskning med det omgivande samhället. Du ska inneha dokumenterad förmåga att erhålla externa medel för forsknings- och utvecklingsprojekt av större omfattning i konkurrens och kunna uppvisa pedagogisk skicklighet genom pedagogiskt utvecklingsarbete och/eller kursutveckling. \n\nDu ska även:\n\n - ha genomgått utbildning i forskarhandledning eller på annat sätt förvärvat motsvarande kunskap;\n - ha dokumenterad och betydande erfarenhet av att ha agerat huvudhandledare på forskarnivå. Handledartiden ska omfatta väsentlig del av forskarutbildningen, i normalfallet två doktorander. I de fall sådan erfarenhet saknas, ska det på annat vis styrkas att kandidaten förvärvat motsvarande kunskaper;\n - kunna uppvisa god förmåga att samverka med omvärlden - nationellt och internationellt;\n - kunna uppvisa god förmåga att planera, organisera och prioritera arbetet på ett effektivt och för ändamålet anpassat sätt samt ha förmåga att hantera resurser på ett sätt som speglar verksamhetens prioriteringar;\n - kunna uppvisa förmåga att leda verksamhet och personal, fatta beslut, ta ansvar samt motivera och förse andra med de förutsättningar som krävs för att effektivt nå gemensamma mål;\n - besitta en god förmåga att uttrycka sig i tal och skrift på svenska och engelska;\n - ha erfarenhet av akademiskt ledarskap.\n\nDet är meriterande om du har ett dokumenterat intresse för hållbarhetsfrågor i forskning och undervisning samt erfarenhet av ämnes- eller forskningsmiljöansvar och dokumenterad erfarenhet av undervisning på svenska och engelska.\n\nFör fullständiga behörighetskrav för en tjänst som professor vid Jönköping University, se tillämpliga delar i våra Bestämmelser för anställning av lärare vid Jönköping University: https://ju.se/om-oss/anställningsordning\n\n \n\nInformation\n\nTjänsten är en tillsvidareanställning på heltid med start maj 2023 -eller enligt överenskommelse. Anställningen inleds normalt med sex månaders provanställning.\nDu kommer erbjudas årsarbetstid för lärare där anställningens villkor regleras i villkorsavtalet och kompletterande kollektivavtal vid Jönköping University. Individuell lönesättning tillämpas.\n\n \n\nAnsökan\n\nVälkommen med din ansökan via vårt rekryteringssystem senast 2022-09-30.\nEventuella bilagor föredras i PDF-format. Du som sökande ansvarar för att ansökan är komplett i enlighet med annonsen och att den är oss tillhanda senast sista ansökningsdag. En ofullständig ansökan kan medföra att din kompetens inte kvalitativt kan prövas i alla avseenden.\n\nAnsökan ska innehålla:\n\n - CV och personligt brev\n - Styrkt meritförteckning\n - Doktorsavhandling\n - Vidimerade intyg och examensbevis\n - Pedagogisk meritportfölj\n - Publikationslista\n - De för tjänsten mest relevanta publikationerna, max 5 st, i fulltext.\n - Tre referenser\n\nJönköping University strävar efter att erbjuda en attraktiv arbetsplats som är fri från diskriminering och ger lika möjligheter för alla. Vi värdesätter de kvaliteter som jämn könsfördelning och mångfald som tillför verksamheten. Vi ser därför gärna sökande av alla kön och med olika födelsebakgrund, funktionalitet och livserfarenhet.\n\nInför rekryteringsarbetet har vi tagit ställning till rekryteringskanaler och marknadsföring. Vi undanber oss därför vänligt men bestämt alla samtal om annonserings- och rekryteringshjälp.\n\nJönköping University är ett modernt lärosäte som kännetecknas av sin internationalisering och entreprenörsanda. Här finns välkvalificerade, uppfinningsrika och företagsamma medarbetare från hela världen. Vi utvecklar ny kunskap och delar kompetens genom hållbara samarbeten med näringsliv, offentlig sektor och andra akademiska institutioner. Vi erbjuder en jämlik och inkluderande arbetsplats och vi välkomnar sökande med olika bakgrund. Läs mer om oss på https://ju.se/",
        "conditions" : "100%. Tillträde: 2023-05-01, eller enligt överenskommelse\r\nTillsvidareanställning",
        "text" : "Högskolan för lärande och kommunikation (HLK) är en av fyra fackhögskolor vid Jönköping University.\r\n\r\nVi bedriver utbildning och forskning inom medie- och kommunikationsvetenskap, internationellt arbete, Human Resources och lärarutbildning, och har en omfattande uppdragsverksamhet riktad mot fortbildning och kompetensutveckling. Hos oss kommer du att få uppleva en kreativ miljö med goda möjligheter till eget inflytande och personlig utveckling.\n\nAvdelningen för kommunikation och beteendevetenskaper rymmer ämnena medie- och kommunikationsvetenskap, psykologi samt forskningsmiljön CHILD. Inom medie- och kommunikationsvetenskap finns utbildningarna Medie- och kommunikationsvetenskapliga programmet (kandidat), Sustainable Communication (internationell magister/master) samt forskarutbildning. Den medie- och kommunikationsvetenskapliga forskningen har haft en internationell prägel och, i linje med utbildningen på avancerad nivå, en profil mot hållbarhet och kommunikation. Det medie- och kommunikationsvetenskapliga kollegiet består av 20-talet medarbetare och inrymmer både akademisk och praktisk kompetens.\n\n \n\nVad erbjuder vi?\n\nSom professor är dina främsta arbetsuppgifter att arbeta med forskning och utveckling av den medie- och kommunikationsvetenskapliga forskningsmiljön samt bidra till den strategiska utvecklingen av forskningen inom fackhögskolan, genom samverkan såväl inom JU som regionalt, nationellt och internationellt. Du förväntas kunna handleda doktorander samt handleda och examinera examensarbeten på grundnivå och avancerad nivå, samt undervisa på de olika medie- och kommunikationsvetenskapliga utbildningarna. I arbetsuppgifterna ingår även utveckling av utbildningar inom ämnet, administrativt arbete samt ämnesreprepresentation i HLK:s forskningsnämnd och utökade ledningsråd.\n\n \n\nVem söker vi?\n\nVi söker dig som har en doktorsexamen inom relevant område och som kan uppvisa en forskningsprofil med tydlig progression efter avhandlingsarbetet vad gäller bredd och djup inom ämnesområdet samt arbeten som är av god internationell standard. Du ska vara vetenskapligt skicklig med avseende på självständiga forskningsinsatser, varit ledare för forskningsgrupp, ha förmåga att planera och leda forskning samt förmåga att kommunicera forskning med det omgivande samhället. Du ska inneha dokumenterad förmåga att erhålla externa medel för forsknings- och utvecklingsprojekt av större omfattning i konkurrens och kunna uppvisa pedagogisk skicklighet genom pedagogiskt utvecklingsarbete och/eller kursutveckling. \n\nDu ska även:\n\n - ha genomgått utbildning i forskarhandledning eller på annat sätt förvärvat motsvarande kunskap;\n - ha dokumenterad och betydande erfarenhet av att ha agerat huvudhandledare på forskarnivå. Handledartiden ska omfatta väsentlig del av forskarutbildningen, i normalfallet två doktorander. I de fall sådan erfarenhet saknas, ska det på annat vis styrkas att kandidaten förvärvat motsvarande kunskaper;\n - kunna uppvisa god förmåga att samverka med omvärlden - nationellt och internationellt;\n - kunna uppvisa god förmåga att planera, organisera och prioritera arbetet på ett effektivt och för ändamålet anpassat sätt samt ha förmåga att hantera resurser på ett sätt som speglar verksamhetens prioriteringar;\n - kunna uppvisa förmåga att leda verksamhet och personal, fatta beslut, ta ansvar samt motivera och förse andra med de förutsättningar som krävs för att effektivt nå gemensamma mål;\n - besitta en god förmåga att uttrycka sig i tal och skrift på svenska och engelska;\n - ha erfarenhet av akademiskt ledarskap.\n\nDet är meriterande om du har ett dokumenterat intresse för hållbarhetsfrågor i forskning och undervisning samt erfarenhet av ämnes- eller forskningsmiljöansvar och dokumenterad erfarenhet av undervisning på svenska och engelska.\n\nFör fullständiga behörighetskrav för en tjänst som professor vid Jönköping University, se tillämpliga delar i våra Bestämmelser för anställning av lärare vid Jönköping University: https://ju.se/om-oss/anställningsordning\n\n \n\nInformation\n\nTjänsten är en tillsvidareanställning på heltid med start maj 2023 -eller enligt överenskommelse. Anställningen inleds normalt med sex månaders provanställning.\nDu kommer erbjudas årsarbetstid för lärare där anställningens villkor regleras i villkorsavtalet och kompletterande kollektivavtal vid Jönköping University. Individuell lönesättning tillämpas.\n\n \n\nAnsökan\n\nVälkommen med din ansökan via vårt rekryteringssystem senast 2022-09-30.\nEventuella bilagor föredras i PDF-format. Du som sökande ansvarar för att ansökan är komplett i enlighet med annonsen och att den är oss tillhanda senast sista ansökningsdag. En ofullständig ansökan kan medföra att din kompetens inte kvalitativt kan prövas i alla avseenden.\n\nAnsökan ska innehålla:\n\n - CV och personligt brev\n - Styrkt meritförteckning\n - Doktorsavhandling\n - Vidimerade intyg och examensbevis\n - Pedagogisk meritportfölj\n - Publikationslista\n - De för tjänsten mest relevanta publikationerna, max 5 st, i fulltext.\n - Tre referenser\n\nJönköping University strävar efter att erbjuda en attraktiv arbetsplats som är fri från diskriminering och ger lika möjligheter för alla. Vi värdesätter de kvaliteter som jämn könsfördelning och mångfald som tillför verksamheten. Vi ser därför gärna sökande av alla kön och med olika födelsebakgrund, funktionalitet och livserfarenhet.\n\nInför rekryteringsarbetet har vi tagit ställning till rekryteringskanaler och marknadsföring. Vi undanber oss därför vänligt men bestämt alla samtal om annonserings- och rekryteringshjälp.\n\nJönköping University är ett modernt lärosäte som kännetecknas av sin internationalisering och entreprenörsanda. Här finns välkvalificerade, uppfinningsrika och företagsamma medarbetare från hela världen. Vi utvecklar ny kunskap och delar kompetens genom hållbara samarbeten med näringsliv, offentlig sektor och andra akademiska institutioner. Vi erbjuder en jämlik och inkluderande arbetsplats och vi välkomnar sökande med olika bakgrund. Läs mer om oss på https://ju.se/"
      },
      "headline" : "Professor i medie- och kommunikationsvetenskap",
      "driving_license" : null,
      "employer" : {
        "email" : null,
        "phone_number" : null,
        "organization_number" : "5564872769",
        "name" : "Högskolan För Lärande och Kommunikation i Jönköp",
        "url" : null,
        "workplace" : "Högskolan för lärande och kommunikation"
      },
      "removed_date" : null,
      "logo_url" : "https://www.arbetsformedlingen.se/rest/arbetsgivare/rest/af/v3/organisation/5564872769/logotyper/logo.png",
      "last_publication_date" : "2022-09-30T23:59:59",
      "number_of_vacancies" : 1,
      "occupation" : {
        "concept_id" : "dT2w_rhK_JtM",
        "label" : "Professor",
        "legacy_ams_taxonomy_id" : "7269"
      },
      "occupation_group" : {
        "concept_id" : "Gm5j_S2Y_aTB",
        "label" : "Professorer",
        "legacy_ams_taxonomy_id" : "2311"
      },
      "employment_type" : {
        "concept_id" : "PFZr_Syz_cUq",
        "label" : "Vanlig anställning",
        "legacy_ams_taxonomy_id" : "1"
      },
      "webpage_url" : "https://arbetsformedlingen.se/platsbanken/annonser/26278983",
      "driving_license_required" : false,
      "application_contacts" : [ {
        "description" : "Ernesto Abalo",
        "email" : null,
        "telephone" : "+4636-101437",
        "name" : null,
        "contact_type" : null
      } ],
      "duration" : {
        "concept_id" : "a7uU_j21_mkL",
        "label" : "Tills vidare",
        "legacy_ams_taxonomy_id" : "1"
      },
      "nice_to_have" : {
        "education_level" : [ ],
        "work_experiences" : [ ],
        "skills" : [ ],
        "education" : [ ],
        "languages" : [ ]
      },
      "application_details" : {
        "via_af" : false,
        "email" : null,
        "other" : null,
        "reference" : "2022/2920-211",
        "url" : "https://ju.varbi.com/what:job/jobID:526246/type:job/where:1/apply:1",
        "information" : null
      },
      "source_type" : "VIA_PLATSBANKEN_DXA",
      "application_deadline" : "2022-09-30T23:59:59",
      "external_id" : "46-556487-2769-526246",
      "workplace_address" : {
        "coordinates" : [ 14.161788, 57.78261 ],
        "city" : null,
        "postcode" : null,
        "municipality" : "Jönköping",
        "region_concept_id" : "MtbE_xWT_eMi",
        "region" : "Jönköpings län",
        "country_code" : "199",
        "region_code" : "06",
        "municipality_concept_id" : "KURg_KJF_Lwc",
        "country_concept_id" : "i46j_HmG_v64",
        "municipality_code" : "0680",
        "street_address" : null,
        "country" : "Sverige"
      },
      "id" : "26278983",
      "experience_required" : true,
      "working_hours_type" : {
        "concept_id" : "6YE1_gAC_R2G",
        "label" : "Heltid",
        "legacy_ams_taxonomy_id" : "1"
      },
      "access" : null,
      "timestamp" : 1656597958322,
      "must_have" : {
        "education_level" : [ ],
        "work_experiences" : [ {
          "concept_id" : "dT2w_rhK_JtM",
          "weight" : 10,
          "label" : "Professor",
          "legacy_ams_taxonomy_id" : "7269"
        } ],
        "skills" : [ ],
        "education" : [ ],
        "languages" : [ ]
      },
      "occupation_field" : {
        "concept_id" : "MVqp_eS8_kDZ",
        "label" : "Pedagogik",
        "legacy_ams_taxonomy_id" : "15"
      },
      "scope_of_work" : {
        "min" : 100,
        "max" : 100
      },
      "salary_type" : {
        "concept_id" : "oG8G_9cW_nRf",
        "label" : "Fast månads- vecko- eller timlön",
        "legacy_ams_taxonomy_id" : "1"
      },
      "removed" : false,
      "salary_description" : "Månadslön",
      "access_to_own_car" : false,
      "publication_date" : "2022-07-04T00:00:00"
    },
    "name" : "Professor i medie- och kommunikationsvetenskap",
    "user" : "alvbj"
  },
  "sha1" : "87fef5a9159988f48f3c2c907f20c486c3b2fe83",
  "annotations" : [ {
    "preferred-label" : "Professor",
    "sentiment" : "MUST_HAVE",
    "type" : "occupation-name",
    "start-position" : 0,
    "end-position" : 9,
    "concept-id" : "dT2w_rhK_JtM",
    "matched-string" : "professor",
    "annotation-id" : 0
  }, {
    "preferred-label" : "Medie- och kommunikationsvetenskap",
    "sentiment" : "MUST_HAVE",
    "type" : "keyword",
    "start-position" : 12,
    "end-position" : 46,
    "concept-id" : "Q5Kq_wDQ_sN4",
    "matched-string" : "medie- och kommunikationsvetenskap",
    "annotation-id" : 1
  }, {
    "preferred-label" : "Medie- och kommunikationsvetenskap",
    "sentiment" : "MUST_HAVE",
    "type" : "keyword",
    "start-position" : 192,
    "end-position" : 226,
    "concept-id" : "Q5Kq_wDQ_sN4",
    "matched-string" : "medie- och kommunikationsvetenskap",
    "annotation-id" : 2
  }, {
    "preferred-label" : "personlig utveckling",
    "sentiment" : "MUST_HAVE",
    "type" : "esco-skill",
    "start-position" : 475,
    "end-position" : 495,
    "concept-id" : "wkQj_Dah_2GD",
    "matched-string" : "personlig utveckling",
    "annotation-id" : 3
  }, {
    "preferred-label" : "Medie- och kommunikationsvetenskap",
    "sentiment" : "MUST_HAVE",
    "type" : "keyword",
    "start-position" : 566,
    "end-position" : 600,
    "concept-id" : "Q5Kq_wDQ_sN4",
    "matched-string" : "medie- och kommunikationsvetenskap",
    "annotation-id" : 4
  }, {
    "preferred-label" : "Psykologi",
    "sentiment" : "MUST_HAVE",
    "type" : "keyword",
    "start-position" : 602,
    "end-position" : 611,
    "concept-id" : "bjR2_XzH_2Py",
    "matched-string" : "psykologi",
    "annotation-id" : 5
  }, {
    "preferred-label" : "Medie- och kommunikationsvetenskap",
    "sentiment" : "MUST_HAVE",
    "type" : "keyword",
    "start-position" : 646,
    "end-position" : 680,
    "concept-id" : "Q5Kq_wDQ_sN4",
    "matched-string" : "medie- och kommunikationsvetenskap",
    "annotation-id" : 6
  }, {
    "preferred-label" : "Forskarutbildning",
    "sentiment" : "MUST_HAVE",
    "type" : "sun-education-level-1",
    "start-position" : 828,
    "end-position" : 845,
    "concept-id" : "ygjL_oek_A2F",
    "matched-string" : "forskarutbildning",
    "annotation-id" : 7
  }, {
    "preferred-label" : "Professor",
    "sentiment" : "MUST_HAVE",
    "type" : "occupation-name",
    "start-position" : 1191,
    "end-position" : 1200,
    "concept-id" : "dT2w_rhK_JtM",
    "matched-string" : "professor",
    "annotation-id" : 8
  }, {
    "preferred-label" : "Doktorander",
    "sentiment" : "MUST_HAVE",
    "type" : "ssyk-level-4",
    "start-position" : 1521,
    "end-position" : 1532,
    "concept-id" : "NNK9_F1o_pK5",
    "matched-string" : "doktorander",
    "annotation-id" : 9
  }, {
    "preferred-label" : "Forskning, erfarenhet",
    "type" : "skill",
    "start-position" : 2134,
    "comment" : "Jag sparar den här för att illustrera att kompetens kommer även i formen av att man efterfrågar yrkesspecifika omdömesförmågor, inte bara instrumentella kompetenser. Dessa förmågor kan säkert pekas mot en eller flera begrepp, eller så måste nya begrepp skapas för dem.",
    "end-position" : 2205,
    "concept-id" : "T5KD_ZPr_ysR",
    "matched-string" : "vetenskapligt skicklig med avseende på självständiga forskningsinsatser",
    "annotation-id" : 22
  }, {
    "preferred-label" : "Forskning, erfarenhet",
    "type" : "skill",
    "start-position" : 2207,
    "comment" : "Jag sparar den här för att illustrera att kompetens kommer även i formen av att man efterfrågar yrkesspecifika omdömesförmågor, inte bara instrumentella kompetenser. Dessa förmågor kan säkert pekas mot en eller flera begrepp, eller så måste nya begrepp skapas för dem.",
    "end-position" : 2239,
    "concept-id" : "T5KD_ZPr_ysR",
    "matched-string" : "varit ledare för forskningsgrupp",
    "annotation-id" : 23
  }, {
    "preferred-label" : "Forskning, erfarenhet",
    "type" : "skill",
    "start-position" : 2256,
    "comment" : "Jag sparar den här för att illustrera att kompetens kommer även i formen av att man efterfrågar yrkesspecifika omdömesförmågor, inte bara instrumentella kompetenser. Dessa förmågor kan säkert pekas mot en eller flera begrepp, eller så måste nya begrepp skapas för dem.",
    "end-position" : 2282,
    "concept-id" : "T5KD_ZPr_ysR",
    "matched-string" : "planera och leda forskning",
    "annotation-id" : 24
  }, {
    "preferred-label" : "Forskning, erfarenhet",
    "type" : "skill",
    "start-position" : 2300,
    "comment" : "Jag sparar den här för att illustrera att kompetens kommer även i formen av att man efterfrågar yrkesspecifika omdömesförmågor, inte bara instrumentella kompetenser. Dessa förmågor kan säkert pekas mot en eller flera begrepp, eller så måste nya begrepp skapas för dem.",
    "end-position" : 2349,
    "concept-id" : "T5KD_ZPr_ysR",
    "matched-string" : "kommunicera forskning med det omgivande samhället",
    "annotation-id" : 25
  }, {
    "preferred-label" : "Forskning, erfarenhet",
    "type" : "skill",
    "start-position" : 2365,
    "comment" : "Jag sparar den här för att illustrera att kompetens kommer även i formen av att man efterfrågar yrkesspecifika omdömesförmågor, inte bara instrumentella kompetenser. Dessa förmågor kan säkert pekas mot en eller flera begrepp, eller så måste nya begrepp skapas för dem.",
    "end-position" : 2484,
    "concept-id" : "T5KD_ZPr_ysR",
    "matched-string" : "dokumenterad förmåga att erhålla externa medel för forsknings- och utvecklingsprojekt av större omfattning i konkurrens",
    "annotation-id" : 26
  }, {
    "preferred-label" : "Pedagogiskt utvecklingsarbete",
    "sentiment" : "MUST_HAVE",
    "type" : "skill",
    "start-position" : 2532,
    "end-position" : 2561,
    "concept-id" : "ZuiC_nRu_EFE",
    "matched-string" : "pedagogiskt utvecklingsarbete",
    "annotation-id" : 10
  }, {
    "preferred-label" : "handledning",
    "type" : "esco-skill",
    "start-position" : 2633,
    "comment" : "Intressant nog finns det handledning som en esco-skill, men den skillen säger nog inte så mycket. Det finns yrkesbenämningen \"forskningshandledare\", så jag skulle nog säga att vissa kunskaper kan bara extraheras om man plockar med yrket som det förekommer tillsammans med, annars fångar man inte det faktiskt efterfrågade kunskapsinnehållet.",
    "end-position" : 2651,
    "concept-id" : "Mgsm_GZo_BEH",
    "matched-string" : "forskarhandledning",
    "annotation-id" : 27
  }, {
    "preferred-label" : "Forskning, erfarenhet",
    "type" : "skill",
    "start-position" : 2754,
    "comment" : "Jag ser egentligen inget problem med att koppla en mening till ett begrepp såhär. Den konceptuella kategorin är godtagbar skulle jag säga. Frågan är bara hur man detekterar en hel mening och kopplar den till en konceptuell kategori, för onekligen är det här en kompetens jag har markerat.",
    "end-position" : 2794,
    "concept-id" : "T5KD_ZPr_ysR",
    "matched-string" : "ha agerat huvudhandledare på forskarnivå",
    "annotation-id" : 28
  }, {
    "preferred-label" : "Doktorander",
    "sentiment" : "MUST_HAVE",
    "type" : "ssyk-level-4",
    "start-position" : 2880,
    "end-position" : 2891,
    "concept-id" : "NNK9_F1o_pK5",
    "matched-string" : "doktorander",
    "annotation-id" : 11
  }, {
    "preferred-label" : null,
    "type" : "skill",
    "start-position" : 3022,
    "comment" : "Det här är en skill, men den är yrkesspecifik, och kan inte ryckas ifrån forskningskontexten helt tror jag. Det är dock inte så att det är \"forskningserfarenhet\" utan snarare en annan kunskap, fast problemområdet är inom forskning. Jag skulle därför gissa att när man säger att något är \"yrkesspecifikt\" såhär, då måste man ha koll på om vi pratar om kunskapsinnehållet eller probleminnehållet är yrkesspecifikt, eller eventuellt både och.",
    "end-position" : 3093,
    "concept-id" : null,
    "matched-string" : "god förmåga att samverka med omvärlden - nationellt och internationellt",
    "annotation-id" : 29
  }, {
    "preferred-label" : "strategisk planering",
    "type" : "esco-skill",
    "start-position" : 3112,
    "comment" : "Det är nog så att man behöver någon slags top-down modell också, eftersom det är sant att det här involverar \"planering\", men verksamhetsområdet, problemområdet är ju \"för forskning\".",
    "end-position" : 3309,
    "concept-id" : "zAmH_7Du_dyN",
    "matched-string" : "god förmåga att planera, organisera och prioritera arbetet på ett effektivt och för ändamålet anpassat sätt samt ha förmåga att hantera resurser på ett sätt som speglar verksamhetens prioriteringar",
    "annotation-id" : 30
  }, {
    "preferred-label" : "Personal",
    "sentiment" : "MUST_HAVE",
    "type" : "keyword",
    "start-position" : 3360,
    "end-position" : 3368,
    "concept-id" : "HVxM_6MS_y1c",
    "matched-string" : "personal",
    "annotation-id" : 12
  }, {
    "preferred-label" : "Svenska",
    "sentiment" : "MUST_HAVE",
    "type" : "language",
    "start-position" : 3557,
    "end-position" : 3564,
    "concept-id" : "zSLA_vw2_FXN",
    "matched-string" : "svenska",
    "annotation-id" : 13
  }, {
    "preferred-label" : "Engelska",
    "sentiment" : "MUST_HAVE",
    "type" : "keyword",
    "start-position" : 3569,
    "end-position" : 3577,
    "concept-id" : "xiD7_snJ_D8x",
    "matched-string" : "engelska",
    "annotation-id" : 14
  }, {
    "preferred-label" : "Undervisning",
    "sentiment" : "MUST_HAVE",
    "type" : "keyword",
    "start-position" : 3715,
    "end-position" : 3727,
    "concept-id" : "1Dtf_9iC_yAN",
    "matched-string" : "undervisning",
    "annotation-id" : 15
  }, {
    "preferred-label" : "Undervisning",
    "sentiment" : "MUST_HAVE",
    "type" : "keyword",
    "start-position" : 3813,
    "end-position" : 3825,
    "concept-id" : "1Dtf_9iC_yAN",
    "matched-string" : "undervisning",
    "annotation-id" : 16
  }, {
    "preferred-label" : "Svenska",
    "sentiment" : "MUST_HAVE",
    "type" : "language",
    "start-position" : 3829,
    "end-position" : 3836,
    "concept-id" : "zSLA_vw2_FXN",
    "matched-string" : "svenska",
    "annotation-id" : 17
  }, {
    "preferred-label" : "Engelska",
    "sentiment" : "MUST_HAVE",
    "type" : "keyword",
    "start-position" : 3841,
    "end-position" : 3849,
    "concept-id" : "xiD7_snJ_D8x",
    "matched-string" : "engelska",
    "annotation-id" : 18
  }, {
    "preferred-label" : "Professor",
    "sentiment" : "MUST_HAVE",
    "type" : "occupation-name",
    "start-position" : 3903,
    "end-position" : 3912,
    "concept-id" : "dT2w_rhK_JtM",
    "matched-string" : "professor",
    "annotation-id" : 19
  }, {
    "preferred-label" : "Kollektivavtal",
    "sentiment" : "MUST_HAVE",
    "type" : "skill",
    "start-position" : 4367,
    "end-position" : 4381,
    "concept-id" : "z7wE_9xb_3jX",
    "matched-string" : "kollektivavtal",
    "annotation-id" : 20
  }, {
    "preferred-label" : "Marknadsföring",
    "sentiment" : "MUST_HAVE",
    "type" : "sun-education-field-4",
    "start-position" : 5497,
    "end-position" : 5511,
    "concept-id" : "zTc5_e4U_Qsf",
    "matched-string" : "marknadsföring",
    "annotation-id" : 21
  } ],
  "text" : "Professor i medie- och kommunikationsvetenskap\nHögskolan för lärande och kommunikation (HLK) är en av fyra fackhögskolor vid Jönköping University.\r\n\r\nVi bedriver utbildning och forskning inom medie- och kommunikationsvetenskap, internationellt arbete, Human Resources och lärarutbildning, och har en omfattande uppdragsverksamhet riktad mot fortbildning och kompetensutveckling. Hos oss kommer du att få uppleva en kreativ miljö med goda möjligheter till eget inflytande och personlig utveckling.\n\nAvdelningen för kommunikation och beteendevetenskaper rymmer ämnena medie- och kommunikationsvetenskap, psykologi samt forskningsmiljön CHILD. Inom medie- och kommunikationsvetenskap finns utbildningarna Medie- och kommunikationsvetenskapliga programmet (kandidat), Sustainable Communication (internationell magister/master) samt forskarutbildning. Den medie- och kommunikationsvetenskapliga forskningen har haft en internationell prägel och, i linje med utbildningen på avancerad nivå, en profil mot hållbarhet och kommunikation. Det medie- och kommunikationsvetenskapliga kollegiet består av 20-talet medarbetare och inrymmer både akademisk och praktisk kompetens.\n\n \n\nVad erbjuder vi?\n\nSom professor är dina främsta arbetsuppgifter att arbeta med forskning och utveckling av den medie- och kommunikationsvetenskapliga forskningsmiljön samt bidra till den strategiska utvecklingen av forskningen inom fackhögskolan, genom samverkan såväl inom JU som regionalt, nationellt och internationellt. Du förväntas kunna handleda doktorander samt handleda och examinera examensarbeten på grundnivå och avancerad nivå, samt undervisa på de olika medie- och kommunikationsvetenskapliga utbildningarna. I arbetsuppgifterna ingår även utveckling av utbildningar inom ämnet, administrativt arbete samt ämnesreprepresentation i HLK:s forskningsnämnd och utökade ledningsråd.\n\n \n\nVem söker vi?\n\nVi söker dig som har en doktorsexamen inom relevant område och som kan uppvisa en forskningsprofil med tydlig progression efter avhandlingsarbetet vad gäller bredd och djup inom ämnesområdet samt arbeten som är av god internationell standard. Du ska vara vetenskapligt skicklig med avseende på självständiga forskningsinsatser, varit ledare för forskningsgrupp, ha förmåga att planera och leda forskning samt förmåga att kommunicera forskning med det omgivande samhället. Du ska inneha dokumenterad förmåga att erhålla externa medel för forsknings- och utvecklingsprojekt av större omfattning i konkurrens och kunna uppvisa pedagogisk skicklighet genom pedagogiskt utvecklingsarbete och/eller kursutveckling. \n\nDu ska även:\n\n - ha genomgått utbildning i forskarhandledning eller på annat sätt förvärvat motsvarande kunskap;\n - ha dokumenterad och betydande erfarenhet av att ha agerat huvudhandledare på forskarnivå. Handledartiden ska omfatta väsentlig del av forskarutbildningen, i normalfallet två doktorander. I de fall sådan erfarenhet saknas, ska det på annat vis styrkas att kandidaten förvärvat motsvarande kunskaper;\n - kunna uppvisa god förmåga att samverka med omvärlden - nationellt och internationellt;\n - kunna uppvisa god förmåga att planera, organisera och prioritera arbetet på ett effektivt och för ändamålet anpassat sätt samt ha förmåga att hantera resurser på ett sätt som speglar verksamhetens prioriteringar;\n - kunna uppvisa förmåga att leda verksamhet och personal, fatta beslut, ta ansvar samt motivera och förse andra med de förutsättningar som krävs för att effektivt nå gemensamma mål;\n - besitta en god förmåga att uttrycka sig i tal och skrift på svenska och engelska;\n - ha erfarenhet av akademiskt ledarskap.\n\nDet är meriterande om du har ett dokumenterat intresse för hållbarhetsfrågor i forskning och undervisning samt erfarenhet av ämnes- eller forskningsmiljöansvar och dokumenterad erfarenhet av undervisning på svenska och engelska.\n\nFör fullständiga behörighetskrav för en tjänst som professor vid Jönköping University, se tillämpliga delar i våra Bestämmelser för anställning av lärare vid Jönköping University: https://ju.se/om-oss/anställningsordning\n\n \n\nInformation\n\nTjänsten är en tillsvidareanställning på heltid med start maj 2023 -eller enligt överenskommelse. Anställningen inleds normalt med sex månaders provanställning.\nDu kommer erbjudas årsarbetstid för lärare där anställningens villkor regleras i villkorsavtalet och kompletterande kollektivavtal vid Jönköping University. Individuell lönesättning tillämpas.\n\n \n\nAnsökan\n\nVälkommen med din ansökan via vårt rekryteringssystem senast 2022-09-30.\nEventuella bilagor föredras i PDF-format. Du som sökande ansvarar för att ansökan är komplett i enlighet med annonsen och att den är oss tillhanda senast sista ansökningsdag. En ofullständig ansökan kan medföra att din kompetens inte kvalitativt kan prövas i alla avseenden.\n\nAnsökan ska innehålla:\n\n - CV och personligt brev\n - Styrkt meritförteckning\n - Doktorsavhandling\n - Vidimerade intyg och examensbevis\n - Pedagogisk meritportfölj\n - Publikationslista\n - De för tjänsten mest relevanta publikationerna, max 5 st, i fulltext.\n - Tre referenser\n\nJönköping University strävar efter att erbjuda en attraktiv arbetsplats som är fri från diskriminering och ger lika möjligheter för alla. Vi värdesätter de kvaliteter som jämn könsfördelning och mångfald som tillför verksamheten. Vi ser därför gärna sökande av alla kön och med olika födelsebakgrund, funktionalitet och livserfarenhet.\n\nInför rekryteringsarbetet har vi tagit ställning till rekryteringskanaler och marknadsföring. Vi undanber oss därför vänligt men bestämt alla samtal om annonserings- och rekryteringshjälp.\n\nJönköping University är ett modernt lärosäte som kännetecknas av sin internationalisering och entreprenörsanda. Här finns välkvalificerade, uppfinningsrika och företagsamma medarbetare från hela världen. Vi utvecklar ny kunskap och delar kompetens genom hållbara samarbeten med näringsliv, offentlig sektor och andra akademiska institutioner. Vi erbjuder en jämlik och inkluderande arbetsplats och vi välkomnar sökande med olika bakgrund. Läs mer om oss på https://ju.se/"
}