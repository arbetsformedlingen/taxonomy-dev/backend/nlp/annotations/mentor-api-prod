{
  "meta" : {
    "jobad" : {
      "description" : {
        "requirements" : null,
        "needs" : null,
        "company_information" : null,
        "text_formatted" : "Är du i början av din karriär och vill jobba med IT-stödsystem som ingår i provning av stridsflygplan JAS 39 Gripen och andra system inom flygarenan? Har du viss erfarenhet av drift och underhåll av IT-system och vill lära dig mer? Då är detta jobbet för dig! FMV upphandlar, utvecklar och levererar materiel och tjänster till det svenska försvaret. I vår vision ”Rätt materiel för ett starkare försvar” ryms vårt arbete och våra leveranser till Försvarsmakten av allt från helikoptrar, ubåtar, stridsflygplan och snörade kängor till kompasser, radarstationer, pansarterrängbilar och kuddar. Bredden i vårt uppdrag ger dig möjligheten att jobba med spännande innehåll, människor och projekt.\n\nOm oss\nFMV:s verksamhetsområde Test och evaluering (T&E) har huvudansvaret för verifiering och validering av Försvarsmaktens tekniska system inom flyg och rymd, sjö, mark och ledning. T&E Luft i Linköping verifierar och validerar förutom stridsflygplan även helikoptrar, obemannade flygande farkoster, UAV, radarspaningsflygplan, transportflygplan samt de stödsystem som tillhör flygsystemen, exempelvis sensorer, beväpning, räddningsutrustning, flygdräkter och motmedelsutrustning. T&E Luft har också en av världens modernaste dynamiska flygsimulatorer med prestanda och realistiska förhållanden som motsvararar moderna stridsflygplan för kvalificering och träning av piloter.\n\nSom myndighet i försvarssektorn är FMVs uppdrag, att leverera det som vårt svenska försvar behöver, viktigare än någonsin. Det innebär att vi behöver växa i antal, bredda vår kompetens och utveckla nya arbetsprocesser. Tillsammans med oss kommer du att arbeta med ett flertal komplexa utmaningar under de kommande åren.\n\nKlicka på länkarna för att se ett klipp och läsa mer om FMV Test och evaluering (fmv.se) och om T&E Luft Linköping (fmv.se).\n\nArbetsuppgifter\nSom junior IT-systemadministratör på T&E Luft är du en viktig del i teamet, som i första hand installerar och konfigurerar stödsystem för JAS 39, men även andra system. Till en början kommer du stöttas av en dedikerad och erfaren kollega i arbetet. När du sen växt in i rollen ser vi att du tar mer eget ansvar i arbetsuppgifterna inom administration och underhåll av servrar, klienter och nätverksutrustning på provplatsen eller hos Försvarsmakten. Rollen innebär även att medverka i utvärdering av stödsystemen till både JAS 39C/D och JAS 39E. På sikt ser vi att du kommer vara med och bidra i framtagande och utvärdering av nya stödsystem för JAS 39.\n\nEn stor andel av de svenskutvecklade stridsflygplanen har utprovats på T&Es provplats i Linköping och det kommande decenniet kommer viktigt arbete genomföras för att verifiera och validera JAS 39 Gripenversioner i nära samarbete med industrin och Försvarsmakten. Som junior IT-systemadministratör är du i kontakt med många parter inom systemstödsteamet och även flygförare, provledare och tekniker. Med tiden kommer du arbeta med att säkerställa att regelverk, rutiner och IT-säkerhet upprätthålls och följs.\n\nResor i tjänsten kan förekomma, såväl inrikes som utrikes.\n\nVi söker dig som\nDå det finns stora möjligheter till utveckling i rollen ser vi att du är i början av din karriär. För att nå framgång i rollen har du kommit i kontakt med drift och underhåll i IT-system baserade på Windows samt nätverk och nätverkstjänster. Du behöver ha systemkunskap inom Windows server/klient operativsystem samt en relevant IT-utbildning eller motsvarande kompetens förvärvad på annat sätt, som vi bedömer likvärdig.\n\nDet är en fördel om du har erfarenhet av en liknande roll samtidigt som du kommer få utbildning när det kommer till systemspecifik kunskap samt flygsystemområdet. Vi tror att ditt tekniska intresse, IT-kunskap och förståelse för hur olika delar samverkar kommer bidra till framgång i rollen som junior systemadministratör på T&E Luft.\n\nVi ser gärna att du har en förståelse för system inom VMware och SQL Datatbas. Det är även en fördel om du har systemkunskap inom nätverksteknik med switchar och routrar. Det är meriterande om du har erfarenhet av att arbeta inom IT-säkerhet och det är positivt om du känner till försvarsmaktens regelverk. Det är däremot inte ett krav, eftersom vi kommer att ge dig utbildning och den miljökunskap som krävs för tjänsten.\n\nVårt projektorienterade arbetssätt förutsätter att du arbetar bra tillsammans med andra. Som person är du stabil och har förmågan att se din roll i olika situationer. För att trivas i rollen är du nyfiken och har en stark vilja att lära dig nytt i samverkan med erfarna kollegor. Du planerar, organiserar och prioriterar ditt arbete på ett effektivt sätt och har förmåga att anpassa dig till ändrade omständigheter. För oss är rätt person viktigt varför vi lägger stor vikt vid dina personliga egenskaper.\n\nDå du kommer delta i arbetsmöten tillsammans med industrin och Försvarsmakten är det en förutsättning att du har en god förmåga att uttrycka dig i tal och skrift, såväl på svenska som på engelska. Givetvis har du B-körkort.\n\nVi erbjuder dig\nPå FMV är du med och bidrar till att göra Sverige och världen säkrare. Vi sätter värde på ömsesidigt förtroende och på ett hållbart arbetsliv. Därför erbjuder vi alla våra anställda ett brett utbud av utbildningar, kompetensutveckling och träning.\n\nTjänsten är placerad i Linköping.\n\nVill du vara med och utveckla framtidens försvar?\nSök tjänsten via FMV:s webbplats. Vi vill ha dina ansökningshandlingar på svenska. Våra fackliga företrädare är SACO Marie Andersson, OFR/S Nicklas Neuman, OFR/O Michael Krüger och SEKO Peter Andersson. Alla nås på telefon 08-782 4000 (FMV Växel). Vid frågor om rollen ber vi dig kontakta Joakim Smith Wennerberg på telefon 073-4474412. Vid frågor om rekryteringsprocessen ber vi dig att kontakta Andrea Molander på andrea.molander@fmv.se.\n\nSista ansökningsdag är 2022-08-29.\n\nVälkommen med din ansökan.\nFMV finns för landet, i många delar av landet. Som medarbetare hos oss får du både en trygg och utmanande anställning och ett stort eget ansvar. För att jobba hos oss behöver du vara svensk medborgare och genomgå en säkerhetsprövning, i enlighet med säkerhetsskyddslagen. Vi tillämpar provanställning och som anställd kommer du att krigsplaceras utifrån totalförsvarets behov. Ansökningar till denna befattning kommer endast tas emot via FMV:s webbplats eller via vår registratur.\n\nVi har redan gjort vårt medieval, därför undanber vi oss samtal från dig som vill sälja annons- och rekryteringstjänster.",
        "conditions" : "Heltid/\r\nEj specificerat",
        "text" : "Är du i början av din karriär och vill jobba med IT-stödsystem som ingår i provning av stridsflygplan JAS 39 Gripen och andra system inom flygarenan? Har du viss erfarenhet av drift och underhåll av IT-system och vill lära dig mer? Då är detta jobbet för dig! FMV upphandlar, utvecklar och levererar materiel och tjänster till det svenska försvaret. I vår vision ”Rätt materiel för ett starkare försvar” ryms vårt arbete och våra leveranser till Försvarsmakten av allt från helikoptrar, ubåtar, stridsflygplan och snörade kängor till kompasser, radarstationer, pansarterrängbilar och kuddar. Bredden i vårt uppdrag ger dig möjligheten att jobba med spännande innehåll, människor och projekt.\n\nOm oss\nFMV:s verksamhetsområde Test och evaluering (T&E) har huvudansvaret för verifiering och validering av Försvarsmaktens tekniska system inom flyg och rymd, sjö, mark och ledning. T&E Luft i Linköping verifierar och validerar förutom stridsflygplan även helikoptrar, obemannade flygande farkoster, UAV, radarspaningsflygplan, transportflygplan samt de stödsystem som tillhör flygsystemen, exempelvis sensorer, beväpning, räddningsutrustning, flygdräkter och motmedelsutrustning. T&E Luft har också en av världens modernaste dynamiska flygsimulatorer med prestanda och realistiska förhållanden som motsvararar moderna stridsflygplan för kvalificering och träning av piloter.\n\nSom myndighet i försvarssektorn är FMVs uppdrag, att leverera det som vårt svenska försvar behöver, viktigare än någonsin. Det innebär att vi behöver växa i antal, bredda vår kompetens och utveckla nya arbetsprocesser. Tillsammans med oss kommer du att arbeta med ett flertal komplexa utmaningar under de kommande åren.\n\nKlicka på länkarna för att se ett klipp och läsa mer om FMV Test och evaluering (fmv.se) och om T&E Luft Linköping (fmv.se).\n\nArbetsuppgifter\nSom junior IT-systemadministratör på T&E Luft är du en viktig del i teamet, som i första hand installerar och konfigurerar stödsystem för JAS 39, men även andra system. Till en början kommer du stöttas av en dedikerad och erfaren kollega i arbetet. När du sen växt in i rollen ser vi att du tar mer eget ansvar i arbetsuppgifterna inom administration och underhåll av servrar, klienter och nätverksutrustning på provplatsen eller hos Försvarsmakten. Rollen innebär även att medverka i utvärdering av stödsystemen till både JAS 39C/D och JAS 39E. På sikt ser vi att du kommer vara med och bidra i framtagande och utvärdering av nya stödsystem för JAS 39.\n\nEn stor andel av de svenskutvecklade stridsflygplanen har utprovats på T&Es provplats i Linköping och det kommande decenniet kommer viktigt arbete genomföras för att verifiera och validera JAS 39 Gripenversioner i nära samarbete med industrin och Försvarsmakten. Som junior IT-systemadministratör är du i kontakt med många parter inom systemstödsteamet och även flygförare, provledare och tekniker. Med tiden kommer du arbeta med att säkerställa att regelverk, rutiner och IT-säkerhet upprätthålls och följs.\n\nResor i tjänsten kan förekomma, såväl inrikes som utrikes.\n\nVi söker dig som\nDå det finns stora möjligheter till utveckling i rollen ser vi att du är i början av din karriär. För att nå framgång i rollen har du kommit i kontakt med drift och underhåll i IT-system baserade på Windows samt nätverk och nätverkstjänster. Du behöver ha systemkunskap inom Windows server/klient operativsystem samt en relevant IT-utbildning eller motsvarande kompetens förvärvad på annat sätt, som vi bedömer likvärdig.\n\nDet är en fördel om du har erfarenhet av en liknande roll samtidigt som du kommer få utbildning när det kommer till systemspecifik kunskap samt flygsystemområdet. Vi tror att ditt tekniska intresse, IT-kunskap och förståelse för hur olika delar samverkar kommer bidra till framgång i rollen som junior systemadministratör på T&E Luft.\n\nVi ser gärna att du har en förståelse för system inom VMware och SQL Datatbas. Det är även en fördel om du har systemkunskap inom nätverksteknik med switchar och routrar. Det är meriterande om du har erfarenhet av att arbeta inom IT-säkerhet och det är positivt om du känner till försvarsmaktens regelverk. Det är däremot inte ett krav, eftersom vi kommer att ge dig utbildning och den miljökunskap som krävs för tjänsten.\n\nVårt projektorienterade arbetssätt förutsätter att du arbetar bra tillsammans med andra. Som person är du stabil och har förmågan att se din roll i olika situationer. För att trivas i rollen är du nyfiken och har en stark vilja att lära dig nytt i samverkan med erfarna kollegor. Du planerar, organiserar och prioriterar ditt arbete på ett effektivt sätt och har förmåga att anpassa dig till ändrade omständigheter. För oss är rätt person viktigt varför vi lägger stor vikt vid dina personliga egenskaper.\n\nDå du kommer delta i arbetsmöten tillsammans med industrin och Försvarsmakten är det en förutsättning att du har en god förmåga att uttrycka dig i tal och skrift, såväl på svenska som på engelska. Givetvis har du B-körkort.\n\nVi erbjuder dig\nPå FMV är du med och bidrar till att göra Sverige och världen säkrare. Vi sätter värde på ömsesidigt förtroende och på ett hållbart arbetsliv. Därför erbjuder vi alla våra anställda ett brett utbud av utbildningar, kompetensutveckling och träning.\n\nTjänsten är placerad i Linköping.\n\nVill du vara med och utveckla framtidens försvar?\nSök tjänsten via FMV:s webbplats. Vi vill ha dina ansökningshandlingar på svenska. Våra fackliga företrädare är SACO Marie Andersson, OFR/S Nicklas Neuman, OFR/O Michael Krüger och SEKO Peter Andersson. Alla nås på telefon 08-782 4000 (FMV Växel). Vid frågor om rollen ber vi dig kontakta Joakim Smith Wennerberg på telefon 073-4474412. Vid frågor om rekryteringsprocessen ber vi dig att kontakta Andrea Molander på andrea.molander@fmv.se.\n\nSista ansökningsdag är 2022-08-29.\n\nVälkommen med din ansökan.\nFMV finns för landet, i många delar av landet. Som medarbetare hos oss får du både en trygg och utmanande anställning och ett stort eget ansvar. För att jobba hos oss behöver du vara svensk medborgare och genomgå en säkerhetsprövning, i enlighet med säkerhetsskyddslagen. Vi tillämpar provanställning och som anställd kommer du att krigsplaceras utifrån totalförsvarets behov. Ansökningar till denna befattning kommer endast tas emot via FMV:s webbplats eller via vår registratur.\n\nVi har redan gjort vårt medieval, därför undanber vi oss samtal från dig som vill sälja annons- och rekryteringstjänster."
      },
      "headline" : "Junior IT-systemadministratör mot stridsflygplan och helikoptrar",
      "driving_license" : [ {
        "concept_id" : "VTK8_WRx_GcM",
        "label" : "B",
        "legacy_ams_taxonomy_id" : "3"
      } ],
      "employer" : {
        "email" : null,
        "phone_number" : null,
        "organization_number" : "2021000340",
        "name" : "Försvarets Materielverk",
        "url" : "http://www.fmv.se",
        "workplace" : "FMV"
      },
      "removed_date" : null,
      "logo_url" : "https://www.arbetsformedlingen.se/rest/arbetsgivare/rest/af/v3/organisation/2021000340/logotyper/logo.png",
      "last_publication_date" : "2022-08-29T23:59:59",
      "number_of_vacancies" : 1,
      "occupation" : {
        "concept_id" : "tdCZ_6Pn_VzT",
        "label" : "IT-tekniker/Datatekniker",
        "legacy_ams_taxonomy_id" : "6625"
      },
      "occupation_group" : {
        "concept_id" : "13md_uyV_BNG",
        "label" : "Drifttekniker, IT",
        "legacy_ams_taxonomy_id" : "3511"
      },
      "employment_type" : {
        "concept_id" : "PFZr_Syz_cUq",
        "label" : "Vanlig anställning",
        "legacy_ams_taxonomy_id" : "1"
      },
      "webpage_url" : "https://arbetsformedlingen.se/platsbanken/annonser/26300780",
      "driving_license_required" : true,
      "application_contacts" : [ ],
      "duration" : {
        "concept_id" : "a7uU_j21_mkL",
        "label" : "Tills vidare",
        "legacy_ams_taxonomy_id" : "1"
      },
      "nice_to_have" : {
        "education_level" : [ ],
        "work_experiences" : [ ],
        "skills" : [ ],
        "education" : [ ],
        "languages" : [ ]
      },
      "application_details" : {
        "via_af" : false,
        "email" : null,
        "other" : null,
        "reference" : null,
        "url" : "https://fmv.attract.reachmee.com/jobs/rm?rmpage=apply&rmjob=4456&ref=https%3A%2F%2Farbetsformedlingen.se%2Fplatsbanken%2F",
        "information" : null
      },
      "source_type" : "VIA_PLATSBANKEN_DXA",
      "application_deadline" : "2022-08-29T23:59:59",
      "external_id" : "46-202100-0340-4456",
      "workplace_address" : {
        "coordinates" : [ 15.621373, 58.41081 ],
        "city" : null,
        "postcode" : null,
        "municipality" : "Linköping",
        "region_concept_id" : "oLT3_Q9p_3nn",
        "region" : "Östergötlands län",
        "country_code" : "199",
        "region_code" : "05",
        "municipality_concept_id" : "bm2x_1mr_Qhx",
        "country_concept_id" : "i46j_HmG_v64",
        "municipality_code" : "0580",
        "street_address" : null,
        "country" : "Sverige"
      },
      "id" : "26300780",
      "experience_required" : true,
      "working_hours_type" : {
        "concept_id" : "6YE1_gAC_R2G",
        "label" : "Heltid",
        "legacy_ams_taxonomy_id" : "1"
      },
      "access" : null,
      "timestamp" : 1657112829230,
      "must_have" : {
        "education_level" : [ ],
        "work_experiences" : [ {
          "concept_id" : "tdCZ_6Pn_VzT",
          "weight" : 10,
          "label" : "IT-tekniker/Datatekniker",
          "legacy_ams_taxonomy_id" : "6625"
        } ],
        "skills" : [ ],
        "education" : [ ],
        "languages" : [ ]
      },
      "occupation_field" : {
        "concept_id" : "apaJ_2ja_LuF",
        "label" : "Data/IT",
        "legacy_ams_taxonomy_id" : "3"
      },
      "scope_of_work" : {
        "min" : 100,
        "max" : 100
      },
      "salary_type" : {
        "concept_id" : "oG8G_9cW_nRf",
        "label" : "Fast månads- vecko- eller timlön",
        "legacy_ams_taxonomy_id" : "1"
      },
      "removed" : false,
      "salary_description" : "Enligt överenskommelse",
      "access_to_own_car" : false,
      "publication_date" : "2022-08-08T00:00:00"
    },
    "name" : "Junior IT-systemadministratör mot stridsflygplan och helikoptrar",
    "user" : "kristof.fischer"
  },
  "sha1" : "ee23adefbe43a1855ad1b585d9d551d5123ff780",
  "annotations" : [ {
    "preferred-label" : "Systemadministratör",
    "sentiment" : "MUST_HAVE",
    "type" : "occupation-name",
    "start-position" : 7,
    "end-position" : 29,
    "concept-id" : "Lxas_8K9_W1o",
    "matched-string" : "it-systemadministratör",
    "annotation-id" : 0
  }, {
    "preferred-label" : "Systemadministratör",
    "sentiment" : "MUST_HAVE",
    "type" : "occupation-name",
    "start-position" : 1911,
    "end-position" : 1933,
    "concept-id" : "Lxas_8K9_W1o",
    "matched-string" : "it-systemadministratör",
    "annotation-id" : 8
  }, {
    "preferred-label" : "Linköping",
    "type" : "municipality",
    "start-position" : 2643,
    "end-position" : 2652,
    "concept-id" : "bm2x_1mr_Qhx",
    "matched-string" : "Linköping",
    "annotation-id" : 30
  }, {
    "preferred-label" : "Systemadministratör",
    "sentiment" : "MUST_HAVE",
    "type" : "occupation-name",
    "start-position" : 2829,
    "end-position" : 2851,
    "concept-id" : "Lxas_8K9_W1o",
    "matched-string" : "it-systemadministratör",
    "annotation-id" : 12
  }, {
    "preferred-label" : "Windows, operativsystem",
    "sentiment" : "MUST_HAVE",
    "type" : "skill",
    "start-position" : 3341,
    "end-position" : 3348,
    "concept-id" : "G4sF_xod_zoq",
    "matched-string" : "windows",
    "annotation-id" : 16
  }, {
    "preferred-label" : "Windows Server, operativsystem",
    "sentiment" : "MUST_HAVE",
    "type" : "skill",
    "start-position" : 3417,
    "end-position" : 3431,
    "concept-id" : "pnX9_gFn_CLt",
    "matched-string" : "Windows server",
    "annotation-id" : 31
  }, {
    "preferred-label" : "Operativsystem",
    "sentiment" : "MUST_HAVE",
    "type" : "skill-headline",
    "start-position" : 3439,
    "end-position" : 3453,
    "concept-id" : "xAWr_WYq_JPP",
    "matched-string" : "operativsystem",
    "annotation-id" : 18
  }, {
    "preferred-label" : "Informations- och kommunikationsteknik (IKT), allmän utbildning",
    "sentiment" : "MUST_HAVE",
    "type" : "sun-education-field-3",
    "start-position" : 3471,
    "end-position" : 3484,
    "concept-id" : "jfpH_VdC_DM3",
    "matched-string" : "IT-utbildning",
    "annotation-id" : 32
  }, {
    "preferred-label" : "Systemadministratör",
    "sentiment" : "MUST_HAVE",
    "type" : "occupation-name",
    "start-position" : 3867,
    "end-position" : 3886,
    "concept-id" : "Lxas_8K9_W1o",
    "matched-string" : "systemadministratör",
    "annotation-id" : 21
  }, {
    "preferred-label" : null,
    "sentiment" : "MUST_HAVE",
    "type" : "skill",
    "start-position" : 3955,
    "end-position" : 3961,
    "concept-id" : null,
    "matched-string" : "VMware",
    "annotation-id" : 33
  }, {
    "preferred-label" : "SQL-Base, databashanterare",
    "sentiment" : "MUST_HAVE",
    "type" : "skill",
    "start-position" : 3966,
    "end-position" : 3978,
    "concept-id" : "r6iV_q4u_a7B",
    "matched-string" : "SQL Datatbas",
    "annotation-id" : 34
  }, {
    "preferred-label" : null,
    "sentiment" : "NICE_TO_HAVE",
    "type" : "skill",
    "start-position" : 4012,
    "end-position" : 4045,
    "concept-id" : null,
    "matched-string" : "systemkunskap inom nätverksteknik",
    "annotation-id" : 35
  }, {
    "preferred-label" : "Säkerhetsfrågor",
    "sentiment" : "NICE_TO_HAVE",
    "type" : "skill",
    "start-position" : 4131,
    "end-position" : 4142,
    "concept-id" : "nu52_VXq_5MD",
    "matched-string" : "it-säkerhet",
    "annotation-id" : 23
  }, {
    "preferred-label" : null,
    "sentiment" : "NICE_TO_HAVE",
    "type" : "skill",
    "start-position" : 4181,
    "end-position" : 4206,
    "concept-id" : null,
    "matched-string" : "försvarsmaktens regelverk",
    "annotation-id" : 36
  }, {
    "preferred-label" : "samarbeta med kolleger",
    "sentiment" : "MUST_HAVE",
    "type" : "esco-skill",
    "start-position" : 4379,
    "end-position" : 4412,
    "concept-id" : "X15R_797_q62",
    "matched-string" : "arbetar bra tillsammans med andra",
    "annotation-id" : 37
  }, {
    "preferred-label" : "Svenska",
    "sentiment" : "MUST_HAVE",
    "type" : "language",
    "start-position" : 5004,
    "end-position" : 5011,
    "concept-id" : "zSLA_vw2_FXN",
    "matched-string" : "svenska",
    "annotation-id" : 25
  }, {
    "preferred-label" : "Engelska",
    "sentiment" : "MUST_HAVE",
    "type" : "language",
    "start-position" : 5019,
    "end-position" : 5027,
    "concept-id" : "NVxJ_hLg_TYS",
    "matched-string" : "engelska",
    "annotation-id" : 26
  }, {
    "preferred-label" : "B",
    "sentiment" : "MUST_HAVE",
    "type" : "driving-licence",
    "start-position" : 5045,
    "end-position" : 5054,
    "concept-id" : "VTK8_WRx_GcM",
    "matched-string" : "B-körkort",
    "annotation-id" : 38
  }, {
    "preferred-label" : "Sverige",
    "type" : "country",
    "start-position" : 5115,
    "end-position" : 5122,
    "concept-id" : "i46j_HmG_v64",
    "matched-string" : "Sverige",
    "annotation-id" : 39
  }, {
    "preferred-label" : "Linköping",
    "type" : "municipality",
    "start-position" : 5345,
    "end-position" : 5354,
    "concept-id" : "bm2x_1mr_Qhx",
    "matched-string" : "Linköping",
    "annotation-id" : 40
  }, {
    "preferred-label" : "Svenskt medborgarskap",
    "sentiment" : "MUST_HAVE",
    "type" : "skill",
    "start-position" : 6094,
    "end-position" : 6111,
    "concept-id" : "chKz_HpD_XL5",
    "matched-string" : "svensk medborgare",
    "annotation-id" : 41
  } ],
  "text" : "Junior IT-systemadministratör mot stridsflygplan och helikoptrar\nÄr du i början av din karriär och vill jobba med IT-stödsystem som ingår i provning av stridsflygplan JAS 39 Gripen och andra system inom flygarenan? Har du viss erfarenhet av drift och underhåll av IT-system och vill lära dig mer? Då är detta jobbet för dig! FMV upphandlar, utvecklar och levererar materiel och tjänster till det svenska försvaret. I vår vision ”Rätt materiel för ett starkare försvar” ryms vårt arbete och våra leveranser till Försvarsmakten av allt från helikoptrar, ubåtar, stridsflygplan och snörade kängor till kompasser, radarstationer, pansarterrängbilar och kuddar. Bredden i vårt uppdrag ger dig möjligheten att jobba med spännande innehåll, människor och projekt.\n\nOm oss\nFMV:s verksamhetsområde Test och evaluering (T&E) har huvudansvaret för verifiering och validering av Försvarsmaktens tekniska system inom flyg och rymd, sjö, mark och ledning. T&E Luft i Linköping verifierar och validerar förutom stridsflygplan även helikoptrar, obemannade flygande farkoster, UAV, radarspaningsflygplan, transportflygplan samt de stödsystem som tillhör flygsystemen, exempelvis sensorer, beväpning, räddningsutrustning, flygdräkter och motmedelsutrustning. T&E Luft har också en av världens modernaste dynamiska flygsimulatorer med prestanda och realistiska förhållanden som motsvararar moderna stridsflygplan för kvalificering och träning av piloter.\n\nSom myndighet i försvarssektorn är FMVs uppdrag, att leverera det som vårt svenska försvar behöver, viktigare än någonsin. Det innebär att vi behöver växa i antal, bredda vår kompetens och utveckla nya arbetsprocesser. Tillsammans med oss kommer du att arbeta med ett flertal komplexa utmaningar under de kommande åren.\n\nKlicka på länkarna för att se ett klipp och läsa mer om FMV Test och evaluering (fmv.se) och om T&E Luft Linköping (fmv.se).\n\nArbetsuppgifter\nSom junior IT-systemadministratör på T&E Luft är du en viktig del i teamet, som i första hand installerar och konfigurerar stödsystem för JAS 39, men även andra system. Till en början kommer du stöttas av en dedikerad och erfaren kollega i arbetet. När du sen växt in i rollen ser vi att du tar mer eget ansvar i arbetsuppgifterna inom administration och underhåll av servrar, klienter och nätverksutrustning på provplatsen eller hos Försvarsmakten. Rollen innebär även att medverka i utvärdering av stödsystemen till både JAS 39C/D och JAS 39E. På sikt ser vi att du kommer vara med och bidra i framtagande och utvärdering av nya stödsystem för JAS 39.\n\nEn stor andel av de svenskutvecklade stridsflygplanen har utprovats på T&Es provplats i Linköping och det kommande decenniet kommer viktigt arbete genomföras för att verifiera och validera JAS 39 Gripenversioner i nära samarbete med industrin och Försvarsmakten. Som junior IT-systemadministratör är du i kontakt med många parter inom systemstödsteamet och även flygförare, provledare och tekniker. Med tiden kommer du arbeta med att säkerställa att regelverk, rutiner och IT-säkerhet upprätthålls och följs.\n\nResor i tjänsten kan förekomma, såväl inrikes som utrikes.\n\nVi söker dig som\nDå det finns stora möjligheter till utveckling i rollen ser vi att du är i början av din karriär. För att nå framgång i rollen har du kommit i kontakt med drift och underhåll i IT-system baserade på Windows samt nätverk och nätverkstjänster. Du behöver ha systemkunskap inom Windows server/klient operativsystem samt en relevant IT-utbildning eller motsvarande kompetens förvärvad på annat sätt, som vi bedömer likvärdig.\n\nDet är en fördel om du har erfarenhet av en liknande roll samtidigt som du kommer få utbildning när det kommer till systemspecifik kunskap samt flygsystemområdet. Vi tror att ditt tekniska intresse, IT-kunskap och förståelse för hur olika delar samverkar kommer bidra till framgång i rollen som junior systemadministratör på T&E Luft.\n\nVi ser gärna att du har en förståelse för system inom VMware och SQL Datatbas. Det är även en fördel om du har systemkunskap inom nätverksteknik med switchar och routrar. Det är meriterande om du har erfarenhet av att arbeta inom IT-säkerhet och det är positivt om du känner till försvarsmaktens regelverk. Det är däremot inte ett krav, eftersom vi kommer att ge dig utbildning och den miljökunskap som krävs för tjänsten.\n\nVårt projektorienterade arbetssätt förutsätter att du arbetar bra tillsammans med andra. Som person är du stabil och har förmågan att se din roll i olika situationer. För att trivas i rollen är du nyfiken och har en stark vilja att lära dig nytt i samverkan med erfarna kollegor. Du planerar, organiserar och prioriterar ditt arbete på ett effektivt sätt och har förmåga att anpassa dig till ändrade omständigheter. För oss är rätt person viktigt varför vi lägger stor vikt vid dina personliga egenskaper.\n\nDå du kommer delta i arbetsmöten tillsammans med industrin och Försvarsmakten är det en förutsättning att du har en god förmåga att uttrycka dig i tal och skrift, såväl på svenska som på engelska. Givetvis har du B-körkort.\n\nVi erbjuder dig\nPå FMV är du med och bidrar till att göra Sverige och världen säkrare. Vi sätter värde på ömsesidigt förtroende och på ett hållbart arbetsliv. Därför erbjuder vi alla våra anställda ett brett utbud av utbildningar, kompetensutveckling och träning.\n\nTjänsten är placerad i Linköping.\n\nVill du vara med och utveckla framtidens försvar?\nSök tjänsten via FMV:s webbplats. Vi vill ha dina ansökningshandlingar på svenska. Våra fackliga företrädare är SACO Marie Andersson, OFR/S Nicklas Neuman, OFR/O Michael Krüger och SEKO Peter Andersson. Alla nås på telefon 08-782 4000 (FMV Växel). Vid frågor om rollen ber vi dig kontakta Joakim Smith Wennerberg på telefon 073-4474412. Vid frågor om rekryteringsprocessen ber vi dig att kontakta Andrea Molander på andrea.molander@fmv.se.\n\nSista ansökningsdag är 2022-08-29.\n\nVälkommen med din ansökan.\nFMV finns för landet, i många delar av landet. Som medarbetare hos oss får du både en trygg och utmanande anställning och ett stort eget ansvar. För att jobba hos oss behöver du vara svensk medborgare och genomgå en säkerhetsprövning, i enlighet med säkerhetsskyddslagen. Vi tillämpar provanställning och som anställd kommer du att krigsplaceras utifrån totalförsvarets behov. Ansökningar till denna befattning kommer endast tas emot via FMV:s webbplats eller via vår registratur.\n\nVi har redan gjort vårt medieval, därför undanber vi oss samtal från dig som vill sälja annons- och rekryteringstjänster."
}